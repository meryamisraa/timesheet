<?php

// src/AppBundle/Admin/EventAdmin.php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class EventAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('user', null, array('label' => 'Utilisateur'))
                ->add('project', null, array('label' => 'Projet'))
                ->add('start_event', 'sonata_type_datetime_picker', array('label' => 'Début', 'attr' => array('class' => 'col-md-6', 'editable' => false)))
                ->add('end_event', 'sonata_type_datetime_picker', array('label' => 'Fin', 'attr' => array('class' => 'col-md-6', 'editable' => false)))
                ->add('done', null, array('label' => 'Réalisé'))
                ->add('todo', null, array('label' => 'Reste à faire'))
                ->add('commentary', null, array('label' => 'Commentaire'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        unset($this->listModes['mosaic']);
        $listMapper
                ->add('user', null, array('label' => 'Utilisateur'))
                ->add('project', null, array('label' => 'Projet'))
                ->add('start_event', 'date', array('label' => 'Jour du mois'))
                //~ ->add('end_event', 'date', array('label' => 'Fin'))
                ->add('done', null, array('label' => 'Réalisé'))
                ->add('todo', null, array('label' => 'Reste à faire'))
                //~ ->add('commentary', null, array('label' => 'Commentaire'))
                
        ;
		$listMapper->add(
            '_action', null, [
				'actions' => [
					//~ 'edit' => [],
					'delete' => [],
					'show' => [],
				]
			]
        );        
    }

    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('user', null, array('label' => 'Utilisateur'))
                ->add('project', null, array('label' => 'Projet'))
                ->add('start_event', 'date', array('label' => 'Jour du mois'))
                //~ ->add('end_event', 'date', array('label' => 'Fin'))
                ->add('done', null, array('label' => 'Réalisé'))
                ->add('todo', null, array('label' => 'Reste à faire'))
                ->add('commentary', null, array('label' => 'Points bloquants'))

		;
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('user', null, array('label' => 'Responsable'))
            ->add('project', null, array('label' => 'Projet'))
            ->add('start_event', 'doctrine_orm_date_range', array('label' => 'Date début'), 
				'sonata_type_date_range_picker', 
				array(
					'field_options_start' => array('format' => 'yyyy-MM-dd'),
					'field_options_end' => array('format' => 'yyyy-MM-dd')
				)
			)
            ->add('end_event', 'doctrine_orm_date_range', array('label' => 'Date fin'), 
				'sonata_type_date_range_picker', 
				array(
					'field_options_start' => array('format' => 'yyyy-MM-dd'),
					'field_options_end' => array('format' => 'yyyy-MM-dd')
				)
			)
		;
    }

}
