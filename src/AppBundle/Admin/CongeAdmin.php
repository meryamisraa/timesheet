<?php

// src/AppBundle/Admin/CongeAdmin.php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CongeAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('user', null, array('label' => 'Utilisateur'))
                ->add('type', null, array('label' => 'Type'))
                ->add('start_conge', 'sonata_type_datetime_picker', array('label' => 'Début', 'attr' => array('class' => 'col-md-6', 'editable' => false)))
                ->add('end_conge', 'sonata_type_datetime_picker', array('label' => 'Fin', 'attr' => array('class' => 'col-md-6', 'editable' => false)))
                ->add('status', null, array('label' => 'Status'))
                ->add('commentary', null, array('label' => 'Commentaire'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        unset($this->listModes['mosaic']);
        $listMapper
                ->add('user', null, array('label' => 'Utilisateur'))
                ->add('start_conge', 'date', array('label' => 'Début'))
                ->add('end_conge', 'date', array('label' => 'Fin'))
                ->add('status', null, array('label' => 'Status'))
        ;
		$listMapper->add(
            '_action', null, [
				'actions' => [
					'edit' => [],
					'delete' => [],
				]
			]
        );
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('user', null, array('label' => 'Utilisateur'))
		;
    }
}
