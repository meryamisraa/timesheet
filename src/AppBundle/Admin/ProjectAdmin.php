<?php

// src/AppBundle/Admin/ProjectAdmin.php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ProjectAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('name', null, array('label' => 'Nom'))
                ->add('client', null, array('label' => 'Client'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        unset($this->listModes['mosaic']);
        $listMapper
                ->add('name', null, array('label' => 'Nom'))
                ->add('client', null, array('label' => 'Client'))
        ;
		$listMapper->add(
            '_action', null, [
				'actions' => [
					'edit' => [],
					'delete' => [],
				]
			]
        );
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'Nom'))
            ->add('client', null, array('label' => 'Client'))
		;
    }

}
