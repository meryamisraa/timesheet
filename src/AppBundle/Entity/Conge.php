<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conge
 *
 * @ORM\Table(name="conge")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CongeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Conge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_conge", type="datetime", nullable=false)
     */
    private $start_conge;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_conge", type="datetime", nullable=false)
     */

    private $end_conge;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="commentary", type="text", nullable=true)
     */
    private $commentary;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \AppBundle\Entity\TypeConge
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeConge", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;
    
    /**
     * Set startConge
     *
     * @param \DateTime $startConge
     *
     * @return Conge
     */
    public function setStartConge($startConge) {
        $this->start_conge = $startConge;

        return $this;
    }

    /**
     * Get startConge
     *
     * @return \DateTime
     */
    public function getStartConge() {
        return $this->start_conge;
    }

    /**
     * Set endConge
     *
     * @param \DateTime $endConge
     *
     * @return Conge
     */
    public function setEndConge($endConge) {
        $this->end_conge = $endConge;

        return $this;
    }

    /**
     * Get endConge
     *
     * @return \DateTime
     */
    public function getEndConge() {
        return $this->end_conge;
    }


    /**
     *
     * @ORM\PrePersist
     */
    public function setCreatedAtValue() {
        if (!$this->getCreatedAt()) {
            $this->created_at = new \DateTime();
            $this->setUpdatedAtValue();
        }
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue() {
        $this->updated_at = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Conge
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Conge
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set commentary
     *
     * @param string $commentary
     *
     * @return Conge
     */
    public function setCommentary($commentary)
    {
        $this->commentary = $commentary;

        return $this;
    }

    /**
     * Get commentary
     *
     * @return string
     */
    public function getCommentary()
    {
        return $this->commentary;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Conge
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\TypeConge $type
     *
     * @return Conge
     */
    public function setType(\AppBundle\Entity\TypeConge $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\TypeConge
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     *
     * @return Conge
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
