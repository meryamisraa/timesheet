<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event", indexes={@ORM\Index(name="IDX_3BAE0AA76B3CA4B", columns={"user_id"}), @ORM\Index(name="IDX_3BAE0AA79122A03F", columns={"project_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Event {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_event", type="datetime", nullable=false)
     */
    private $start_event;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_event", type="datetime", nullable=true)
     */
    private $end_event;

    /**
     * @var string
     *
     * @ORM\Column(name="done", type="text", nullable=false)
     */
    private $done;
    /**
     * @var string
     *
     * @ORM\Column(name="todo", type="text", nullable=false)
     */
    private $todo;
    /**
     * @var string
     *
     * @ORM\Column(name="commentary", type="text", nullable=true)
     */
    private $commentary;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Project
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;
    
    public function __toString() {
        return $this->getUser() ? $this->getUser()->getUsername() : '-';
    }
    /**
     *
     * @ORM\PrePersist
     */
    public function setCreatedAtValue() {
        if (!$this->getCreatedAt()) {
            $this->created_at = new \DateTime();
            $this->setUpdatedAtValue();
        }
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue() {
        $this->updated_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set startEvent
     *
     * @param \DateTime $startEvent
     *
     * @return Event
     */
    public function setStartEvent($startEvent) {
        $this->start_event = $startEvent;

        return $this;
    }

    /**
     * Get startEvent
     *
     * @return \DateTime
     */
    public function getStartEvent() {
        return $this->start_event;
    }

    /**
     * Set endEvent
     *
     * @param \DateTime $endEvent
     *
     * @return Event
     */
    public function setEndEvent($endEvent) {
        $this->end_event = $endEvent;

        return $this;
    }

    /**
     * Get endEvent
     *
     * @return \DateTime
     */
    public function getEndEvent() {
        return $this->end_event;
    }

    /**
     * Set commentary
     *
     * @param string $commentary
     *
     * @return Event
     */
    public function setCommentary($commentary) {
        $this->commentary = $commentary;

        return $this;
    }

    /**
     * Get commentary
     *
     * @return string
     */
    public function getCommentary() {
        return $this->commentary;
    }
    
    /**
     * Set done
     *
     * @param string $done
     *
     * @return Event
     */
    public function setDone($done) {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return string
     */
    public function getDone() {
        return $this->done;
    }
    
    /**
     * Set todo
     *
     * @param string $todo
     *
     * @return Event
     */
    public function setToDo($todo) {
        $this->todo = $todo;

        return $this;
    }

    /**
     * Get todo
     *
     * @return string
     */
    public function getToDo() {
        return $this->todo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Event
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Event
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     *
     * @return Event
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return Event
     */
    public function setProject(\AppBundle\Entity\Project $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject() {
        return $this->project;
    }

}
