<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse as JsonResponse;
use \DateTime;

/**
 * Event controller.
 *
 * @Route("event")
 */
class EventController extends Controller {

    /**
     * Lists all event entities.
     *
     * @Route("/", name="event_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('AppBundle:Event')->findAll();

        return $this->render('event/index.html.twig', array(
                    'events' => $events,
        ));
    }
    
	/**
     * Lists all event entities.
     *
     * @Route("/events/load", name="events_load")
     * @Method("GET")
     */
    public function loadAction(Request $request) {
		$user = $this->container->get('security.context')->getToken()->getUser();
		$em = $this->getDoctrine()->getManager();
		
		$data = array();
		
		$sql = 'SELECT e FROM AppBundle:Event e ';
		if (!$this->isGranted('ROLE_ABSHORE_ADMIN')) {
			$sql .= 'WHERE e.user = ' . $user->getId();
		}
		
        $query = $em->createQuery($sql);
		$result = $query->getResult();
		foreach($result as $row){
			$data[] = array(
			  'id'   => $row->getId(),
			  'title'   => $row->getProject()->getName(),
			  'start'   => $row->getStartEvent()->format('Y-m-d h:i:s'),
			  'end'   => $row->getEndEvent()->format('Y-m-d h:i:s'),
			  'color' => 'red'
			 );
		}
		
		//~ requette des congé + couleur change suivant le status.

		return new JsonResponse($data);
    }

    /**
     * Creates a new event entity.
     *
     * @Route("/new/{start}/{end}", name="event_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $start, $end) {
        $event = new Event();
        
        $event->setStartEvent(new DateTime($start));
        $event->setEndEvent(new DateTime($start));
        
        $form = $this->createForm('AppBundle\Form\EventType', $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
			$user = $this->container->get('security.context')
			->getToken()
			->getUser();
			
            $event->setCreatedAtValue();
            $event->setUser($user);
            
            $em->persist($event);
            $em->flush();

            return new JsonResponse(array('id' => $event->getId()));
        }

        return $this->render('event/new.html.twig', array(
                    'event' => $event,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a event entity.
     *
     * @Route("/{id}", name="event_show")
     * @Method("GET")
     */
    public function showAction(Event $event) {
        $deleteForm = $this->createDeleteForm($event);

        return $this->render('event/show.html.twig', array(
                    'event' => $event,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing event entity.
     *
     * @Route("/{id}/edit", name="event_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Event $event) {
        $deleteForm = $this->createDeleteForm($event);
        $editForm = $this->createForm('AppBundle\Form\EventType', $event);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('event_edit', array('id' => $event->getId()));
        }

        return $this->render('event/edit.html.twig', array(
                    'event' => $event,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a event entity.
     *
     * @Route("/{id}", name="event_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Event $event) {
        $form = $this->createDeleteForm($event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }

        return $this->redirectToRoute('stats_list');
    }

    /**
     * Creates a form to delete a event entity.
     *
     * @param Event $event The event entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Event $event) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('event_delete', array('id' => $event->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
