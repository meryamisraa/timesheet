<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Entity\Catalogue;
use AppBundle\Entity\Brand;
use AppBundle\Entity\Material;
use AppBundle\Entity\Kind;
use AppBundle\Entity\Type;
use AppBundle\Entity\Form;
use AppBundle\Entity\Company;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\UserBundle\Entity\User;
use XMLReader;
use DOMDocument;


class ExportCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        // Name and description for app/console command
        $this
            ->setName('app:export')
            ->setDescription('Permet d\'exporter les consommés')
            ->setHelp('Je serai affiche si on lance la commande app/console app:export -h');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Showing when the script is launched
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        // Importing CSV on DB via Doctrine ORM
        $this->export($input, $output);

        // Showing when the script is over
        $now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
    }

    public function secure_rand($min, $max)
    {
        return (unpack("N", openssl_random_pseudo_bytes(4)) % ($max - $min)) + $min;
    }

    protected function export(InputInterface $input, OutputInterface $output)
    {
        // Getting php array of data from CSV
        $data = $this->get($input, $output);

		// Getting doctrine manager
		$em = $this->getContainer()->get('doctrine')->getManager();
		// Turning off doctrine default logs queries for saving memory
		$em->getConnection()->getConfiguration()->setSQLLogger(null);

        // Define the size of record, the frequency for persisting the data and the current index of records
        $size = count($data);
        $batchSize = 20;
        $i = 1;

        // Starting progress
        $progress = new ProgressBar($output, $size);
        $progress->start();

        // Processing on each row of data
        foreach ($data as $row) {


            // Each 20 frames persisted we flush everything
            if (($i % $batchSize) === 0) {

                $em->flush();
                // Detaches all objects from Doctrine for memory save
                $em->clear();

                // Advancing for progress display on console
                $progress->advance($batchSize);

                $now = new \DateTime();
                $output->writeln(' of object imported ... | ' . $now->format('d-m-Y G:i:s'));
            }

            $i++;
        }

        // Flushing and clear data on queue
        $em->flush();
        $em->clear();

        // Ending the progress bar process
        $progress->finish();
    }

    protected function get(InputInterface $input, OutputInterface $output)
    {
        // Getting doctrine manager
        $em = $this->getContainer()->get('doctrine')->getManager();
        // Turning off doctrine default logs queries for saving memory
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $data = array();
        $query = $em->createQuery(
			'SELECT e
			FROM AppBundle:Event e'
		);
		$result = $query->getResult();
		foreach($result as $row){
			$data[$row->getUser()->getUsername()][$row->getProject()->getName()] = 0;
		}
		
		return $data;

    }

}
