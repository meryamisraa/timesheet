<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class EventType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
			->add('start_event', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
					'class' => 'js-datepicker form-control'
				]
			))
			->add('end_event', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
					'class' => 'js-datepicker form-control'
				]
			))
			->add('project', null, array('required' => true))
			->add('done', null, array('required' => true))
			->add('todo', null, array('required' => true))
			->add('commentary', null, array('required' => true))
			;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Event'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_event';
    }

}
