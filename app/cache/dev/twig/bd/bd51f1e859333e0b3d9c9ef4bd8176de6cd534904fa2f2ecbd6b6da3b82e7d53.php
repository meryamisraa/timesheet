<?php

/* AppBundle::stats.html.twig */
class __TwigTemplate_1e04376351afdebea7dcb016af97fc24ae17f13524f2e465c81b69f22417234e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SonataAdminBundle::layout.html.twig", "AppBundle::stats.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::stats.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "
<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" role=\"dialog\">
    <div class=\"modal-dialog\">
        <!-- Modal content-->
        <div class=\"modal-content\">

        </div>
    </div>
</div>
    <div id=\"calendar\"></div>
    <script>

        \$(document).ready(function () {
            var calendar = \$('#calendar').fullCalendar({
                editable: false,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: \"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("events_load");
        echo "\",
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay)
                {
\t\t\t\t\tvar start = \$.fullCalendar.formatDate(start, \"Y-MM-DD HH:mm:ss\");
\t\t\t\t\tvar end = \$.fullCalendar.formatDate(end, \"Y-MM-DD HH:mm:ss\");
\t\t\t\t\t
\t\t\t\t\tvar url = \"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("event_new", array("start" => "__START__", "end" => "__END__")), "html", null, true);
        echo "\";
\t\t\t\t\turl = url.replace('__START__', encodeURIComponent(start));
\t\t\t\t\turl = url.replace('__END__', encodeURIComponent(end));
\t\t\t\t
                    \$('.modal-content').load(url, function () {
\t\t\t\t\t\t//Date picker
\t\t\t\t\t\t\$('.js-datepicker').datetimepicker({
\t\t\t\t\t\t\tformat: 'Y-MM-DD'
\t\t\t\t\t\t});
\t\t\t\t\t\t\$('#myModal').modal({ show: true });
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\"#event-form\").submit(function(event){
\t\t\t\t\t\t\tvar request_method = \$(this).attr(\"method\"); //get form GET/POST method
\t\t\t\t\t\t\tvar form_data = \$(this).serialize(); //Encode form elements for submission
\t\t\t\t\t\t\tevent.preventDefault(); //prevent default action 
\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\turl : url,
\t\t\t\t\t\t\t\ttype: request_method,
\t\t\t\t\t\t\t\tdata : form_data
\t\t\t\t\t\t\t}).done(function(response){
\t\t\t\t\t\t\t\t\$('#myModal').modal('hide');
\t\t\t\t\t\t\t\tcalendar.fullCalendar('refetchEvents');
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t\t\t\t});
                },
                editable: true,
                eventClick: function (event)
                {
\t\t\t\t\tvar id = event.id;
\t\t\t\t\tvar url = \"";
        // line 63
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("event_edit", array("id" => "__ID__"));
        echo "\";
\t\t\t\t\turl = url.replace('__ID__', id);
\t\t\t\t
                    \$('.modal-content').load(url, function () {
\t\t\t\t\t\t
\t\t\t\t\t\t//Date picker
\t\t\t\t\t\t\$('.js-datepicker').datetimepicker({
\t\t\t\t\t\t\tformat: 'Y-MM-DD'
\t\t\t\t\t\t});
\t\t\t\t\t\t
\t\t\t\t\t\t\$('#myModal').modal({ show: true });
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\"#event-form\").submit(function(event){
\t\t\t\t\t\t\tvar request_method = \$(this).attr(\"method\"); //get form GET/POST method
\t\t\t\t\t\t\tvar form_data = \$(this).serialize(); //Encode form elements for submission
\t\t\t\t\t\t\tevent.preventDefault(); //prevent default action 
\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\turl : url,
\t\t\t\t\t\t\t\ttype: request_method,
\t\t\t\t\t\t\t\tdata : form_data
\t\t\t\t\t\t\t}).done(function(response){
\t\t\t\t\t\t\t\t\$('#myModal').modal('hide');
\t\t\t\t\t\t\t\tcalendar.fullCalendar('refetchEvents');
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t\t\t\t});
                },

            });
        });

    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::stats.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 63,  74 => 33,  63 => 25,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'SonataAdminBundle::layout.html.twig' %}

{% block content %}

<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" role=\"dialog\">
    <div class=\"modal-dialog\">
        <!-- Modal content-->
        <div class=\"modal-content\">

        </div>
    </div>
</div>
    <div id=\"calendar\"></div>
    <script>

        \$(document).ready(function () {
            var calendar = \$('#calendar').fullCalendar({
                editable: false,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: \"{{ path( 'events_load' ) }}\",
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay)
                {
\t\t\t\t\tvar start = \$.fullCalendar.formatDate(start, \"Y-MM-DD HH:mm:ss\");
\t\t\t\t\tvar end = \$.fullCalendar.formatDate(end, \"Y-MM-DD HH:mm:ss\");
\t\t\t\t\t
\t\t\t\t\tvar url = \"{{ path('event_new' , { start : '__START__', 'end' : '__END__' })}}\";
\t\t\t\t\turl = url.replace('__START__', encodeURIComponent(start));
\t\t\t\t\turl = url.replace('__END__', encodeURIComponent(end));
\t\t\t\t
                    \$('.modal-content').load(url, function () {
\t\t\t\t\t\t//Date picker
\t\t\t\t\t\t\$('.js-datepicker').datetimepicker({
\t\t\t\t\t\t\tformat: 'Y-MM-DD'
\t\t\t\t\t\t});
\t\t\t\t\t\t\$('#myModal').modal({ show: true });
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\"#event-form\").submit(function(event){
\t\t\t\t\t\t\tvar request_method = \$(this).attr(\"method\"); //get form GET/POST method
\t\t\t\t\t\t\tvar form_data = \$(this).serialize(); //Encode form elements for submission
\t\t\t\t\t\t\tevent.preventDefault(); //prevent default action 
\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\turl : url,
\t\t\t\t\t\t\t\ttype: request_method,
\t\t\t\t\t\t\t\tdata : form_data
\t\t\t\t\t\t\t}).done(function(response){
\t\t\t\t\t\t\t\t\$('#myModal').modal('hide');
\t\t\t\t\t\t\t\tcalendar.fullCalendar('refetchEvents');
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t\t\t\t});
                },
                editable: true,
                eventClick: function (event)
                {
\t\t\t\t\tvar id = event.id;
\t\t\t\t\tvar url = \"{{ path('event_edit' , { id : '__ID__' }) }}\";
\t\t\t\t\turl = url.replace('__ID__', id);
\t\t\t\t
                    \$('.modal-content').load(url, function () {
\t\t\t\t\t\t
\t\t\t\t\t\t//Date picker
\t\t\t\t\t\t\$('.js-datepicker').datetimepicker({
\t\t\t\t\t\t\tformat: 'Y-MM-DD'
\t\t\t\t\t\t});
\t\t\t\t\t\t
\t\t\t\t\t\t\$('#myModal').modal({ show: true });
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\"#event-form\").submit(function(event){
\t\t\t\t\t\t\tvar request_method = \$(this).attr(\"method\"); //get form GET/POST method
\t\t\t\t\t\t\tvar form_data = \$(this).serialize(); //Encode form elements for submission
\t\t\t\t\t\t\tevent.preventDefault(); //prevent default action 
\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\turl : url,
\t\t\t\t\t\t\t\ttype: request_method,
\t\t\t\t\t\t\t\tdata : form_data
\t\t\t\t\t\t\t}).done(function(response){
\t\t\t\t\t\t\t\t\$('#myModal').modal('hide');
\t\t\t\t\t\t\t\tcalendar.fullCalendar('refetchEvents');
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t\t\t\t});
                },

            });
        });

    </script>

{% endblock %}
", "AppBundle::stats.html.twig", "/var/www/html/timesheet/src/AppBundle/Resources/views/stats.html.twig");
    }
}
