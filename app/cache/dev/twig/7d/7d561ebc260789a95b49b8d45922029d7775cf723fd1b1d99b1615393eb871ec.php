<?php

/* SonataAdminBundle::side_bar_nav.html.twig */
class __TwigTemplate_d06acb595efce164dd3889b6a000af66ab74d5babb21187e18c22bee770a9e1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::side_bar_nav.html.twig"));

        // line 1
        echo "<ul class=\"sidebar-menu\">
\t";
        // line 2
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ABSHORE_DEV")) {
            // line 3
            echo "        <li class=\"";
            if (twig_in_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), array(0 => "stats_list"))) {
                echo "active ";
            }
            echo "treeview\">
            <a href=\"#\"><i class=\"fa fa-folder\"></i><span>Mon espace</span><span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span></a>
            <ul class=\"treeview-menu menu_level_1\">
                <li class=\"";
            // line 6
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "stats_list")) {
                echo "active ";
            }
            echo "first\">
                    <a href=\"";
            // line 7
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("stats_list");
            echo "\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>TimeSheet</a>
                </li>
            </ul>
        </li>
\t";
        }
        // line 12
        echo "
\t";
        // line 13
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ABSHORE_ADMIN")) {
            // line 14
            echo "        <li class=\"";
            if (twig_in_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), array(0 => "admin_app_typeconge_list", 1 => "admin_sonata_user_user_list", 2 => "admin_app_client_list", 3 => "admin_app_project_list"))) {
                echo "active ";
            }
            echo "treeview\">
            <a href=\"#\"><i class=\"fa fa-folder\"></i><span>Paramètrage</span><span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span></a>
            <ul class=\"treeview-menu menu_level_1\">
\t\t\t\t<li class=\"";
            // line 17
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "admin_sonata_user_user_list")) {
                echo "active ";
            }
            echo "first\">
\t\t\t\t\t<a href=\"";
            // line 18
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_sonata_user_user_list");
            echo "\"><i class=\"fa fa-angle-double-right\"></i>Utilisateurs</a>
\t\t\t\t</li>                
\t\t\t\t<li class=\"";
            // line 20
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "admin_app_client_list")) {
                echo "active ";
            }
            echo "first\">
                    <a href=\"";
            // line 21
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_client_list");
            echo "\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Clients</a>
                </li>
                <li class=\"";
            // line 23
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "admin_app_project_list")) {
                echo "active ";
            }
            echo "first\">
                    <a href=\"";
            // line 24
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_project_list");
            echo "\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Projets</a>
                </li>

                <li class=\"";
            // line 27
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "admin_app_typeconge_list")) {
                echo "active ";
            }
            echo "first\">
                    <a href=\"";
            // line 28
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_typeconge_list");
            echo "\">
                        <i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Type de congés</a>
                </li>

            </ul>
        </li>
        
        <li class=\"";
            // line 35
            if (twig_in_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), array(0 => "admin_app_conge_list", 1 => "stats_list", 2 => "admin_app_event_list"))) {
                echo "active ";
            }
            echo "treeview\">
            <a href=\"#\"><i class=\"fa fa-folder\"></i><span>Global</span><span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span></a>
            <ul class=\"treeview-menu menu_level_1\">
                <li class=\"";
            // line 38
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "admin_app_event_list")) {
                echo "active ";
            }
            echo "first\">
                    <a href=\"";
            // line 39
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_event_list");
            echo "\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>TimeSheet</a>
                </li>
                <li class=\"";
            // line 41
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "stats_list")) {
                echo "active ";
            }
            echo "first\">
                    <a href=\"";
            // line 42
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("stats_list");
            echo "\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Agenda</a>
                </li>
                <li class=\"";
            // line 44
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "admin_app_conge_list")) {
                echo "active ";
            }
            echo "first\">
                    <a href=\"";
            // line 45
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_conge_list");
            echo "\">
                        <i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Congés</a>
                </li>
            </ul>
        </li>
\t";
        }
        // line 51
        echo "</ul>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::side_bar_nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 51,  150 => 45,  144 => 44,  139 => 42,  133 => 41,  128 => 39,  122 => 38,  114 => 35,  104 => 28,  98 => 27,  92 => 24,  86 => 23,  81 => 21,  75 => 20,  70 => 18,  64 => 17,  55 => 14,  53 => 13,  50 => 12,  42 => 7,  36 => 6,  27 => 3,  25 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<ul class=\"sidebar-menu\">
\t{% if is_granted('ROLE_ABSHORE_DEV') %}
        <li class=\"{% if app.request.attributes.get('_route') in ['stats_list'] %}active {% endif %}treeview\">
            <a href=\"#\"><i class=\"fa fa-folder\"></i><span>Mon espace</span><span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span></a>
            <ul class=\"treeview-menu menu_level_1\">
                <li class=\"{% if app.request.attributes.get('_route') == 'stats_list' %}active {% endif %}first\">
                    <a href=\"{{ path('stats_list') }}\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>TimeSheet</a>
                </li>
            </ul>
        </li>
\t{% endif %}

\t{% if is_granted('ROLE_ABSHORE_ADMIN') %}
        <li class=\"{% if app.request.attributes.get('_route') in ['admin_app_typeconge_list', 'admin_sonata_user_user_list', 'admin_app_client_list', 'admin_app_project_list'] %}active {% endif %}treeview\">
            <a href=\"#\"><i class=\"fa fa-folder\"></i><span>Paramètrage</span><span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span></a>
            <ul class=\"treeview-menu menu_level_1\">
\t\t\t\t<li class=\"{% if app.request.attributes.get('_route') == 'admin_sonata_user_user_list' %}active {% endif %}first\">
\t\t\t\t\t<a href=\"{{ path('admin_sonata_user_user_list') }}\"><i class=\"fa fa-angle-double-right\"></i>Utilisateurs</a>
\t\t\t\t</li>                
\t\t\t\t<li class=\"{% if app.request.attributes.get('_route') == 'admin_app_client_list' %}active {% endif %}first\">
                    <a href=\"{{ path('admin_app_client_list') }}\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Clients</a>
                </li>
                <li class=\"{% if app.request.attributes.get('_route') == 'admin_app_project_list' %}active {% endif %}first\">
                    <a href=\"{{ path('admin_app_project_list') }}\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Projets</a>
                </li>

                <li class=\"{% if app.request.attributes.get('_route') == 'admin_app_typeconge_list' %}active {% endif %}first\">
                    <a href=\"{{ path('admin_app_typeconge_list') }}\">
                        <i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Type de congés</a>
                </li>

            </ul>
        </li>
        
        <li class=\"{% if app.request.attributes.get('_route') in ['admin_app_conge_list', 'stats_list', 'admin_app_event_list'] %}active {% endif %}treeview\">
            <a href=\"#\"><i class=\"fa fa-folder\"></i><span>Global</span><span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span></a>
            <ul class=\"treeview-menu menu_level_1\">
                <li class=\"{% if app.request.attributes.get('_route') == 'admin_app_event_list' %}active {% endif %}first\">
                    <a href=\"{{ path('admin_app_event_list') }}\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>TimeSheet</a>
                </li>
                <li class=\"{% if app.request.attributes.get('_route') == 'stats_list' %}active {% endif %}first\">
                    <a href=\"{{ path('stats_list') }}\"><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Agenda</a>
                </li>
                <li class=\"{% if app.request.attributes.get('_route') == 'admin_app_conge_list' %}active {% endif %}first\">
                    <a href=\"{{ path('admin_app_conge_list') }}\">
                        <i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>Congés</a>
                </li>
            </ul>
        </li>
\t{% endif %}
</ul>
", "SonataAdminBundle::side_bar_nav.html.twig", "/var/www/html/timesheet/app/Resources/SonataAdminBundle/views/side_bar_nav.html.twig");
    }
}
