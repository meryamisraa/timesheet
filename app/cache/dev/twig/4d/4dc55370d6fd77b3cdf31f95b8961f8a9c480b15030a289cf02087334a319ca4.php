<?php

/* event/new.html.twig */
class __TwigTemplate_8b40d63fe174522252163c2a387d6d47ef456eaa0e9d11567770c0dfa58eda7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "event/new.html.twig"));

        // line 1
        echo "<div class=\"modal-header\">
\t<h4 class=\"modal-title\">Imputer TimeSheet</h4>
</div>

";
        // line 5
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontal", "id" => "event-form")));
        echo "

<div class=\"modal-body\">
\t
        ";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'widget');
        echo "
        
        <div class=\"form-group\">
\t\t\t<label for=\"appbundle_rfq_deadline\" class=\"col-sm-2 control-label\">Date début</label>
\t\t\t<div class=\"col-sm-10\">
\t\t\t\t<div class=\"input-group date\">
\t\t\t\t\t<div class=\"input-group-addon\">
\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i>
\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "start_event", array()), 'widget', array("attr" => array("placeholder" => "Date début", "value" => twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "getStartEvent", array(), "method"), "Y-m-d"))));
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
        <div class=\"form-group\">
\t\t\t<label for=\"appbundle_rfq_deadline\" class=\"col-sm-2 control-label\">Date fin</label>
\t\t\t<div class=\"col-sm-10\">
\t\t\t\t<div class=\"input-group date\">
\t\t\t\t\t<div class=\"input-group-addon\">
\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i>
\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "end_event", array()), 'widget', array("attr" => array("placeholder" => "Date fin", "value" => twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : $this->getContext($context, "event")), "getEndEvent", array(), "method"), "Y-m-d"))));
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
        <div class=\"form-group\">
            <label for=\"inputarticle\" class=\"col-sm-2 control-label\">Projet</label>
            <div class=\"col-sm-10\">
                ";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "project", array()), 'widget', array("attr" => array("class" => "", "placeholder" => "Projet")));
        echo "
            </div>
        </div>
        <div class=\"form-group\">
            <label for=\"inputarticle\" class=\"col-sm-2 control-label\">Done</label>
            <div class=\"col-sm-10\">
                ";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "done", array()), 'widget', array("attr" => array("class" => "", "placeholder" => "Tâches réalisées")));
        echo "
            </div>
        </div>
        <div class=\"form-group\">
            <label for=\"inputarticle\" class=\"col-sm-2 control-label\">To do</label>
            <div class=\"col-sm-10\">
                ";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "todo", array()), 'widget', array("attr" => array("class" => "", "placeholder" => "Tâches restantes")));
        echo "
            </div>
        </div>        <div class=\"form-group\">
            <label for=\"inputarticle\" class=\"col-sm-2 control-label\">Commentaire</label>
            <div class=\"col-sm-10\">
                ";
        // line 53
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentary", array()), 'widget', array("attr" => array("class" => "", "placeholder" => "Points bloquants")));
        echo "
            </div>
        </div>
        
        
    <div class=\"\">                
        ";
        // line 59
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
    </div>
</div>

<div class=\"modal-footer\">
\t<button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Close</button>
\t<input class=\"btn btn-info\" id=\"event-target-button\" type=\"submit\" value=\"Créer\" />
</div>

";
        // line 68
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "event/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 68,  106 => 59,  97 => 53,  89 => 48,  80 => 42,  71 => 36,  61 => 29,  47 => 18,  35 => 9,  28 => 5,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"modal-header\">
\t<h4 class=\"modal-title\">Imputer TimeSheet</h4>
</div>

{{ form_start(form, { 'attr': { 'class': 'form-horizontal', 'id': 'event-form' } }) }}

<div class=\"modal-body\">
\t
        {{ form_widget(form._token) }}
        
        <div class=\"form-group\">
\t\t\t<label for=\"appbundle_rfq_deadline\" class=\"col-sm-2 control-label\">Date début</label>
\t\t\t<div class=\"col-sm-10\">
\t\t\t\t<div class=\"input-group date\">
\t\t\t\t\t<div class=\"input-group-addon\">
\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i>
\t\t\t\t\t</div>
\t\t\t\t\t{{ form_widget(form.start_event, { 'attr': { 'placeholder': 'Date début', value : event.getStartEvent()|date(\"Y-m-d\") } }) }}
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
        <div class=\"form-group\">
\t\t\t<label for=\"appbundle_rfq_deadline\" class=\"col-sm-2 control-label\">Date fin</label>
\t\t\t<div class=\"col-sm-10\">
\t\t\t\t<div class=\"input-group date\">
\t\t\t\t\t<div class=\"input-group-addon\">
\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i>
\t\t\t\t\t</div>
\t\t\t\t\t{{ form_widget(form.end_event, { 'attr': { 'placeholder': 'Date fin', value : event.getEndEvent()|date(\"Y-m-d\") } }) }}
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
        <div class=\"form-group\">
            <label for=\"inputarticle\" class=\"col-sm-2 control-label\">Projet</label>
            <div class=\"col-sm-10\">
                {{ form_widget(form.project, { 'attr': {'class': '', 'placeholder': 'Projet' } }) }}
            </div>
        </div>
        <div class=\"form-group\">
            <label for=\"inputarticle\" class=\"col-sm-2 control-label\">Done</label>
            <div class=\"col-sm-10\">
                {{ form_widget(form.done, { 'attr': {'class': '', 'placeholder': 'Tâches réalisées' } }) }}
            </div>
        </div>
        <div class=\"form-group\">
            <label for=\"inputarticle\" class=\"col-sm-2 control-label\">To do</label>
            <div class=\"col-sm-10\">
                {{ form_widget(form.todo, { 'attr': {'class': '', 'placeholder': 'Tâches restantes' } }) }}
            </div>
        </div>        <div class=\"form-group\">
            <label for=\"inputarticle\" class=\"col-sm-2 control-label\">Commentaire</label>
            <div class=\"col-sm-10\">
                {{ form_widget(form.commentary, { 'attr': {'class': '', 'placeholder': 'Points bloquants' } }) }}
            </div>
        </div>
        
        
    <div class=\"\">                
        {{ form_rest(form) }}
    </div>
</div>

<div class=\"modal-footer\">
\t<button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Close</button>
\t<input class=\"btn btn-info\" id=\"event-target-button\" type=\"submit\" value=\"Créer\" />
</div>

{{ form_end(form) }}
", "event/new.html.twig", "/var/www/html/timesheet/app/Resources/views/event/new.html.twig");
    }
}
