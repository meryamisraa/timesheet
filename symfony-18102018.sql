-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Jeu 18 Octobre 2018 à 16:14
-- Version du serveur :  5.7.23-0ubuntu0.18.04.1
-- Version de PHP :  5.6.38-1+ubuntu18.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `symfony`
--

-- --------------------------------------------------------

--
-- Structure de la table `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `acl_classes`
--

INSERT INTO `acl_classes` (`id`, `class_type`) VALUES
(8, 'AppBundle\\Admin\\ClientAdmin'),
(7, 'AppBundle\\Admin\\EventAdmin'),
(9, 'AppBundle\\Admin\\ProjectAdmin'),
(6, 'AppBundle\\Admin\\StatsAdmin'),
(1, 'AppBundle\\Entity\\Client'),
(3, 'AppBundle\\Entity\\Event'),
(2, 'AppBundle\\Entity\\Project'),
(4, 'Application\\Sonata\\UserBundle\\Entity\\Group'),
(5, 'Application\\Sonata\\UserBundle\\Entity\\User'),
(10, 'Sonata\\ClassificationBundle\\Admin\\CategoryAdmin'),
(12, 'Sonata\\ClassificationBundle\\Admin\\CollectionAdmin'),
(13, 'Sonata\\ClassificationBundle\\Admin\\ContextAdmin'),
(11, 'Sonata\\ClassificationBundle\\Admin\\TagAdmin'),
(15, 'Sonata\\MediaBundle\\Admin\\GalleryAdmin'),
(16, 'Sonata\\MediaBundle\\Admin\\GalleryHasMediaAdmin'),
(14, 'Sonata\\MediaBundle\\Admin\\ORM\\MediaAdmin'),
(18, 'Sonata\\NewsBundle\\Admin\\CommentAdmin'),
(17, 'Sonata\\NewsBundle\\Admin\\PostAdmin'),
(20, 'Sonata\\UserBundle\\Admin\\Entity\\GroupAdmin'),
(19, 'Sonata\\UserBundle\\Admin\\Entity\\UserAdmin');

-- --------------------------------------------------------

--
-- Structure de la table `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `security_identity_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) UNSIGNED NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `acl_entries`
--

INSERT INTO `acl_entries` (`id`, `class_id`, `object_identity_id`, `security_identity_id`, `field_name`, `ace_order`, `mask`, `granting`, `granting_strategy`, `audit_success`, `audit_failure`) VALUES
(1, 1, NULL, 1, NULL, 0, 64, 1, 'all', 0, 0),
(2, 1, NULL, 2, NULL, 1, 32, 1, 'all', 0, 0),
(3, 1, NULL, 3, NULL, 2, 4, 1, 'all', 0, 0),
(4, 1, NULL, 4, NULL, 3, 1, 1, 'all', 0, 0),
(5, 1, 1, 5, NULL, 0, 128, 1, 'all', 0, 0),
(6, 1, 2, 5, NULL, 0, 128, 1, 'all', 0, 0),
(7, 2, NULL, 6, NULL, 0, 64, 1, 'all', 0, 0),
(8, 2, NULL, 7, NULL, 1, 32, 1, 'all', 0, 0),
(9, 2, NULL, 8, NULL, 2, 4, 1, 'all', 0, 0),
(10, 2, NULL, 9, NULL, 3, 1, 1, 'all', 0, 0),
(11, 2, 3, 5, NULL, 0, 128, 1, 'all', 0, 0),
(12, 2, 4, 5, NULL, 0, 128, 1, 'all', 0, 0),
(13, 2, 5, 5, NULL, 0, 128, 1, 'all', 0, 0),
(14, 3, NULL, 10, NULL, 0, 64, 1, 'all', 0, 0),
(15, 3, NULL, 11, NULL, 1, 32, 1, 'all', 0, 0),
(16, 3, NULL, 12, NULL, 2, 4, 1, 'all', 0, 0),
(17, 3, NULL, 13, NULL, 3, 1, 1, 'all', 0, 0),
(18, 3, 6, 5, NULL, 0, 128, 1, 'all', 0, 0),
(19, 4, NULL, 14, NULL, 0, 64, 1, 'all', 0, 0),
(20, 4, NULL, 15, NULL, 1, 32, 1, 'all', 0, 0),
(21, 4, NULL, 16, NULL, 2, 4, 1, 'all', 0, 0),
(22, 4, NULL, 17, NULL, 3, 1, 1, 'all', 0, 0),
(23, 4, 7, 5, NULL, 0, 128, 1, 'all', 0, 0),
(24, 4, 8, 5, NULL, 0, 128, 1, 'all', 0, 0),
(25, 5, NULL, 18, NULL, 0, 64, 1, 'all', 0, 0),
(26, 5, NULL, 19, NULL, 1, 32, 1, 'all', 0, 0),
(27, 5, NULL, 20, NULL, 2, 4, 1, 'all', 0, 0),
(28, 5, NULL, 21, NULL, 3, 1, 1, 'all', 0, 0),
(29, 5, 9, 5, NULL, 0, 128, 1, 'all', 0, 0),
(30, 5, 10, 5, NULL, 0, 128, 1, 'all', 0, 0),
(31, 6, NULL, 22, NULL, 0, 64, 1, 'all', 0, 0),
(32, 6, NULL, 23, NULL, 1, 8224, 1, 'all', 0, 0),
(33, 6, NULL, 24, NULL, 2, 4098, 1, 'all', 0, 0),
(34, 6, NULL, 25, NULL, 3, 4096, 1, 'all', 0, 0),
(35, 7, NULL, 10, NULL, 0, 64, 1, 'all', 0, 0),
(36, 7, NULL, 11, NULL, 1, 8224, 1, 'all', 0, 0),
(37, 7, NULL, 12, NULL, 2, 4098, 1, 'all', 0, 0),
(38, 7, NULL, 13, NULL, 3, 4096, 1, 'all', 0, 0),
(39, 8, NULL, 1, NULL, 0, 64, 1, 'all', 0, 0),
(40, 8, NULL, 2, NULL, 1, 8224, 1, 'all', 0, 0),
(41, 8, NULL, 3, NULL, 2, 4098, 1, 'all', 0, 0),
(42, 8, NULL, 4, NULL, 3, 4096, 1, 'all', 0, 0),
(43, 9, NULL, 6, NULL, 0, 64, 1, 'all', 0, 0),
(44, 9, NULL, 7, NULL, 1, 8224, 1, 'all', 0, 0),
(45, 9, NULL, 8, NULL, 2, 4098, 1, 'all', 0, 0),
(46, 9, NULL, 9, NULL, 3, 4096, 1, 'all', 0, 0),
(47, 10, NULL, 26, NULL, 0, 64, 1, 'all', 0, 0),
(48, 10, NULL, 27, NULL, 1, 8224, 1, 'all', 0, 0),
(49, 10, NULL, 28, NULL, 2, 4098, 1, 'all', 0, 0),
(50, 10, NULL, 29, NULL, 3, 4096, 1, 'all', 0, 0),
(51, 11, NULL, 30, NULL, 0, 64, 1, 'all', 0, 0),
(52, 11, NULL, 31, NULL, 1, 8224, 1, 'all', 0, 0),
(53, 11, NULL, 32, NULL, 2, 4098, 1, 'all', 0, 0),
(54, 11, NULL, 33, NULL, 3, 4096, 1, 'all', 0, 0),
(55, 12, NULL, 34, NULL, 0, 64, 1, 'all', 0, 0),
(56, 12, NULL, 35, NULL, 1, 8224, 1, 'all', 0, 0),
(57, 12, NULL, 36, NULL, 2, 4098, 1, 'all', 0, 0),
(58, 12, NULL, 37, NULL, 3, 4096, 1, 'all', 0, 0),
(59, 13, NULL, 38, NULL, 0, 64, 1, 'all', 0, 0),
(60, 13, NULL, 39, NULL, 1, 8224, 1, 'all', 0, 0),
(61, 13, NULL, 40, NULL, 2, 4098, 1, 'all', 0, 0),
(62, 13, NULL, 41, NULL, 3, 4096, 1, 'all', 0, 0),
(63, 14, NULL, 42, NULL, 0, 64, 1, 'all', 0, 0),
(64, 14, NULL, 43, NULL, 1, 8224, 1, 'all', 0, 0),
(65, 14, NULL, 44, NULL, 2, 4098, 1, 'all', 0, 0),
(66, 14, NULL, 45, NULL, 3, 4096, 1, 'all', 0, 0),
(67, 15, NULL, 46, NULL, 0, 64, 1, 'all', 0, 0),
(68, 15, NULL, 47, NULL, 1, 8224, 1, 'all', 0, 0),
(69, 15, NULL, 48, NULL, 2, 4098, 1, 'all', 0, 0),
(70, 15, NULL, 49, NULL, 3, 4096, 1, 'all', 0, 0),
(71, 16, NULL, 50, NULL, 0, 64, 1, 'all', 0, 0),
(72, 16, NULL, 51, NULL, 1, 8224, 1, 'all', 0, 0),
(73, 16, NULL, 52, NULL, 2, 4098, 1, 'all', 0, 0),
(74, 16, NULL, 53, NULL, 3, 4096, 1, 'all', 0, 0),
(75, 17, NULL, 54, NULL, 0, 64, 1, 'all', 0, 0),
(76, 17, NULL, 55, NULL, 1, 8224, 1, 'all', 0, 0),
(77, 17, NULL, 56, NULL, 2, 4098, 1, 'all', 0, 0),
(78, 17, NULL, 57, NULL, 3, 4096, 1, 'all', 0, 0),
(79, 18, NULL, 58, NULL, 0, 64, 1, 'all', 0, 0),
(80, 18, NULL, 59, NULL, 1, 8224, 1, 'all', 0, 0),
(81, 18, NULL, 60, NULL, 2, 4098, 1, 'all', 0, 0),
(82, 18, NULL, 61, NULL, 3, 4096, 1, 'all', 0, 0),
(83, 19, NULL, 18, NULL, 0, 64, 1, 'all', 0, 0),
(84, 19, NULL, 19, NULL, 1, 8224, 1, 'all', 0, 0),
(85, 19, NULL, 20, NULL, 2, 4098, 1, 'all', 0, 0),
(86, 19, NULL, 21, NULL, 3, 4096, 1, 'all', 0, 0),
(87, 20, NULL, 14, NULL, 0, 64, 1, 'all', 0, 0),
(88, 20, NULL, 15, NULL, 1, 8224, 1, 'all', 0, 0),
(89, 20, NULL, 16, NULL, 2, 4098, 1, 'all', 0, 0),
(90, 20, NULL, 17, NULL, 3, 4096, 1, 'all', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `acl_object_identities`
--

INSERT INTO `acl_object_identities` (`id`, `parent_object_identity_id`, `class_id`, `object_identifier`, `entries_inheriting`) VALUES
(1, NULL, 1, '1', 1),
(2, NULL, 1, '2', 1),
(3, NULL, 2, '1', 1),
(4, NULL, 2, '2', 1),
(5, NULL, 2, '3', 1),
(6, NULL, 3, '1', 1),
(7, NULL, 4, '1', 1),
(8, NULL, 4, '2', 1),
(9, NULL, 5, '2', 1),
(10, NULL, 5, '3', 1),
(11, NULL, 6, 'bundle.admin.stats', 1),
(12, NULL, 7, 'admin.events', 1),
(13, NULL, 8, 'admin.clients', 1),
(14, NULL, 9, 'admin.projects', 1),
(15, NULL, 10, 'sonata.classification.admin.category', 1),
(16, NULL, 11, 'sonata.classification.admin.tag', 1),
(17, NULL, 12, 'sonata.classification.admin.collection', 1),
(18, NULL, 13, 'sonata.classification.admin.context', 1),
(19, NULL, 14, 'sonata.media.admin.media', 1),
(20, NULL, 15, 'sonata.media.admin.gallery', 1),
(21, NULL, 16, 'sonata.media.admin.gallery_has_media', 1),
(22, NULL, 17, 'sonata.news.admin.post', 1),
(23, NULL, 18, 'sonata.news.admin.comment', 1),
(24, NULL, 19, 'sonata.user.admin.user', 1),
(25, NULL, 20, 'sonata.user.admin.group', 1);

-- --------------------------------------------------------

--
-- Structure de la table `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) UNSIGNED NOT NULL,
  `ancestor_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `acl_object_identity_ancestors`
--

INSERT INTO `acl_object_identity_ancestors` (`object_identity_id`, `ancestor_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(24, 24),
(25, 25);

-- --------------------------------------------------------

--
-- Structure de la table `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `acl_security_identities`
--

INSERT INTO `acl_security_identities` (`id`, `identifier`, `username`) VALUES
(5, 'Application\\Sonata\\UserBundle\\Entity\\User-admin', 1),
(1, 'ROLE_ADMIN_CLIENTS_ADMIN', 0),
(2, 'ROLE_ADMIN_CLIENTS_EDITOR', 0),
(4, 'ROLE_ADMIN_CLIENTS_GUEST', 0),
(3, 'ROLE_ADMIN_CLIENTS_STAFF', 0),
(10, 'ROLE_ADMIN_EVENTS_ADMIN', 0),
(11, 'ROLE_ADMIN_EVENTS_EDITOR', 0),
(13, 'ROLE_ADMIN_EVENTS_GUEST', 0),
(12, 'ROLE_ADMIN_EVENTS_STAFF', 0),
(6, 'ROLE_ADMIN_PROJECTS_ADMIN', 0),
(7, 'ROLE_ADMIN_PROJECTS_EDITOR', 0),
(9, 'ROLE_ADMIN_PROJECTS_GUEST', 0),
(8, 'ROLE_ADMIN_PROJECTS_STAFF', 0),
(22, 'ROLE_BUNDLE_ADMIN_STATS_ADMIN', 0),
(23, 'ROLE_BUNDLE_ADMIN_STATS_EDITOR', 0),
(25, 'ROLE_BUNDLE_ADMIN_STATS_GUEST', 0),
(24, 'ROLE_BUNDLE_ADMIN_STATS_STAFF', 0),
(26, 'ROLE_SONATA_CLASSIFICATION_ADMIN_CATEGORY_ADMIN', 0),
(27, 'ROLE_SONATA_CLASSIFICATION_ADMIN_CATEGORY_EDITOR', 0),
(29, 'ROLE_SONATA_CLASSIFICATION_ADMIN_CATEGORY_GUEST', 0),
(28, 'ROLE_SONATA_CLASSIFICATION_ADMIN_CATEGORY_STAFF', 0),
(34, 'ROLE_SONATA_CLASSIFICATION_ADMIN_COLLECTION_ADMIN', 0),
(35, 'ROLE_SONATA_CLASSIFICATION_ADMIN_COLLECTION_EDITOR', 0),
(37, 'ROLE_SONATA_CLASSIFICATION_ADMIN_COLLECTION_GUEST', 0),
(36, 'ROLE_SONATA_CLASSIFICATION_ADMIN_COLLECTION_STAFF', 0),
(38, 'ROLE_SONATA_CLASSIFICATION_ADMIN_CONTEXT_ADMIN', 0),
(39, 'ROLE_SONATA_CLASSIFICATION_ADMIN_CONTEXT_EDITOR', 0),
(41, 'ROLE_SONATA_CLASSIFICATION_ADMIN_CONTEXT_GUEST', 0),
(40, 'ROLE_SONATA_CLASSIFICATION_ADMIN_CONTEXT_STAFF', 0),
(30, 'ROLE_SONATA_CLASSIFICATION_ADMIN_TAG_ADMIN', 0),
(31, 'ROLE_SONATA_CLASSIFICATION_ADMIN_TAG_EDITOR', 0),
(33, 'ROLE_SONATA_CLASSIFICATION_ADMIN_TAG_GUEST', 0),
(32, 'ROLE_SONATA_CLASSIFICATION_ADMIN_TAG_STAFF', 0),
(46, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_ADMIN', 0),
(47, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_EDITOR', 0),
(49, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_GUEST', 0),
(50, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_ADMIN', 0),
(51, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_EDITOR', 0),
(53, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_GUEST', 0),
(52, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_STAFF', 0),
(48, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_STAFF', 0),
(42, 'ROLE_SONATA_MEDIA_ADMIN_MEDIA_ADMIN', 0),
(43, 'ROLE_SONATA_MEDIA_ADMIN_MEDIA_EDITOR', 0),
(45, 'ROLE_SONATA_MEDIA_ADMIN_MEDIA_GUEST', 0),
(44, 'ROLE_SONATA_MEDIA_ADMIN_MEDIA_STAFF', 0),
(58, 'ROLE_SONATA_NEWS_ADMIN_COMMENT_ADMIN', 0),
(59, 'ROLE_SONATA_NEWS_ADMIN_COMMENT_EDITOR', 0),
(61, 'ROLE_SONATA_NEWS_ADMIN_COMMENT_GUEST', 0),
(60, 'ROLE_SONATA_NEWS_ADMIN_COMMENT_STAFF', 0),
(54, 'ROLE_SONATA_NEWS_ADMIN_POST_ADMIN', 0),
(55, 'ROLE_SONATA_NEWS_ADMIN_POST_EDITOR', 0),
(57, 'ROLE_SONATA_NEWS_ADMIN_POST_GUEST', 0),
(56, 'ROLE_SONATA_NEWS_ADMIN_POST_STAFF', 0),
(14, 'ROLE_SONATA_USER_ADMIN_GROUP_ADMIN', 0),
(15, 'ROLE_SONATA_USER_ADMIN_GROUP_EDITOR', 0),
(17, 'ROLE_SONATA_USER_ADMIN_GROUP_GUEST', 0),
(16, 'ROLE_SONATA_USER_ADMIN_GROUP_STAFF', 0),
(18, 'ROLE_SONATA_USER_ADMIN_USER_ADMIN', 0),
(19, 'ROLE_SONATA_USER_ADMIN_USER_EDITOR', 0),
(21, 'ROLE_SONATA_USER_ADMIN_USER_GUEST', 0),
(20, 'ROLE_SONATA_USER_ADMIN_USER_STAFF', 0);

-- --------------------------------------------------------

--
-- Structure de la table `classification__category`
--

CREATE TABLE `classification__category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `classification__category`
--

INSERT INTO `classification__category` (`id`, `parent_id`, `context`, `media_id`, `name`, `enabled`, `slug`, `description`, `position`, `created_at`, `updated_at`) VALUES
(1, NULL, 'default', NULL, 'default', 1, 'default', 'default', NULL, '2018-10-12 14:18:13', '2018-10-12 14:18:13');

-- --------------------------------------------------------

--
-- Structure de la table `classification__collection`
--

CREATE TABLE `classification__collection` (
  `id` int(11) NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `classification__context`
--

CREATE TABLE `classification__context` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `classification__context`
--

INSERT INTO `classification__context` (`id`, `name`, `enabled`, `created_at`, `updated_at`) VALUES
('default', 'default', 1, '2018-10-12 14:18:12', '2018-10-12 14:18:12');

-- --------------------------------------------------------

--
-- Structure de la table `classification__tag`
--

CREATE TABLE `classification__tag` (
  `id` int(11) NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'FittingBox', NULL, NULL),
(2, 'UPCAR', NULL, NULL),
(3, 'C\'EVIDENTIA', NULL, NULL),
(4, 'ABSHORE', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `conge`
--

CREATE TABLE `conge` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_conge` datetime NOT NULL,
  `end_conge` datetime NOT NULL,
  `commentary` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `start_event` datetime NOT NULL,
  `end_event` datetime DEFAULT NULL,
  `commentary` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `done` longtext COLLATE utf8_unicode_ci NOT NULL,
  `todo` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `event`
--

INSERT INTO `event` (`id`, `user_id`, `project_id`, `start_event`, `end_event`, `commentary`, `created_at`, `updated_at`, `done`, `todo`) VALUES
(2, 10, 8, '2018-10-16 00:00:00', '2018-10-15 00:00:00', 'Commencer le projet de nouveau vu les confusions produites', NULL, NULL, 'RAS', 'RAS'),
(5, 6, 5, '2018-10-01 00:00:00', '2018-10-01 00:00:00', 'intégration des maquettes Fitmetrix | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(6, 10, 5, '2018-10-01 00:00:00', '2018-10-01 00:00:00', 'Intégration html css', NULL, NULL, 'RAS', 'RAS'),
(7, 6, 5, '2018-10-02 00:00:00', '2018-10-02 00:00:00', 'intégration des maquettes Fitmetrix | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(8, 6, 5, '2018-10-03 00:00:00', '2018-10-03 00:00:00', 'intégration des maquettes Fitmetrix | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(12, 10, 5, '2018-10-02 00:00:00', '2018-10-02 00:00:00', 'Intégration html css', NULL, NULL, 'RAS', 'RAS'),
(14, 10, 5, '2018-10-03 00:00:00', '2018-10-03 00:00:00', 'Réaliser le fichier de traduction json', NULL, NULL, 'RAS', 'RAS'),
(15, 10, 8, '2018-10-04 00:00:00', '2018-10-04 00:00:00', 'Compréhension du projet de gestion des congés', NULL, NULL, 'RAS', 'RAS'),
(19, 10, 10, '2018-10-05 00:00:00', '2018-10-06 00:00:00', 'Prise d\'un jour de congé pour aller chercher ma diplôme', NULL, NULL, 'RAS', 'RAS'),
(20, 6, 5, '2018-10-04 00:00:00', '2018-10-04 00:00:00', 'intégration des maquettes Fitmetrix | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(21, 6, 5, '2018-10-05 00:00:00', '2018-10-05 00:00:00', 'intégration des maquettes Fitmetrix | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(22, 9, 5, '2018-10-01 00:00:00', '2018-10-01 00:00:00', 'intégration et développement  Front-office d’authentification', NULL, NULL, 'RAS', 'RAS'),
(23, 6, 5, '2018-10-08 00:00:00', '2018-10-08 00:00:00', 'intégration des maquettes Fitmetrix | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(24, 6, 5, '2018-10-09 00:00:00', '2018-10-09 00:00:00', 'intégration des maquettes Fitmetrix  + creation des webservices ( Restful API ) qui envoie des requettes JSON  ( Les 3 images ) | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(25, 6, 5, '2018-10-10 00:00:00', '2018-10-10 00:00:00', 'intégration des maquettes Fitmetrix  ( Angualr6) | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(26, 6, 5, '2018-10-11 00:00:00', '2018-10-11 00:00:00', 'intégration des maquettes Fitmetrix  | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(27, 6, 5, '2018-10-12 00:00:00', '2018-10-12 00:00:00', 'intégration des maquettes Fitmetrix | fitmetrix-28\r\n la correction de quelques retours qui sont fournis par Ahmed :\r\nhttps://docs.google.com/spreadsheets/d/1R7460gFTjr9yspmyc9v5CnbU7mvTNljk4MtVFPGzcIY/edit?usp=sharing', NULL, NULL, 'RAS', 'RAS'),
(28, 6, 9, '2018-10-15 00:00:00', '2018-10-15 00:00:00', 'Congé sans solde', NULL, NULL, 'RAS', 'RAS'),
(29, 9, 5, '2018-10-02 00:00:00', '2018-10-02 00:00:00', 'développement front-end l’interface  guide d’utilisation', NULL, NULL, 'RAS', 'RAS'),
(30, 6, 4, '2018-10-16 00:00:00', '2018-10-16 00:00:00', 'Déboguer  le probléme de session Fitmetrix BackOffice ( Symfony2.0) | fitmetrix-28', NULL, NULL, 'RAS', 'RAS'),
(31, 9, 5, '2018-10-03 00:00:00', '2018-10-03 00:00:00', 'développement  front -end de CSS  personnalisé', NULL, NULL, 'RAS', 'RAS'),
(32, 9, 5, '2018-10-04 00:00:00', '2018-10-04 00:00:00', 'développement  front -end de CSS  personnalisé', NULL, NULL, 'RAS', 'RAS'),
(33, 9, 5, '2018-10-05 00:00:00', '2018-10-05 00:00:00', 'développement  front -end de CSS  personnalisé', NULL, NULL, 'RAS', 'RAS'),
(34, 9, 5, '2018-10-08 00:00:00', '2018-10-08 00:00:00', 'développement  front -end de langue personnalisé', NULL, NULL, 'RAS', 'RAS'),
(35, 10, 8, '2018-10-08 00:00:00', '2018-10-08 00:00:00', 'Création du projet gestion_conges \r\nInstallation et configuration du php et symfony', NULL, NULL, 'RAS', 'RAS'),
(36, 10, 8, '2018-10-09 00:00:00', '2018-10-09 00:00:00', 'Installation du virtual hosts \r\nInstallation du fosuserbundle', NULL, NULL, 'RAS', 'RAS'),
(37, 9, 5, '2018-10-09 00:00:00', '2018-10-09 00:00:00', 'développement  front -end de langue personnalisé', NULL, NULL, 'RAS', 'RAS'),
(38, 10, 8, '2018-10-15 00:00:00', '2018-10-15 00:00:00', 'Installation de sonata admin', NULL, NULL, 'RAS', 'RAS'),
(39, 9, 5, '2018-10-10 00:00:00', '2018-10-10 00:00:00', 'développement  front -end de configuration webcam', NULL, NULL, 'RAS', 'RAS'),
(40, 9, 5, '2018-10-11 00:00:00', '2018-10-11 00:00:00', 'développement  front -end de  captures de  3 images et envoie les images vers back-end utlisant  les api', NULL, NULL, 'RAS', 'RAS'),
(41, 9, 5, '2018-10-12 00:00:00', '2018-10-12 00:00:00', 'test et validation', NULL, NULL, 'RAS', 'RAS'),
(43, 9, 10, '2018-10-15 00:00:00', '2018-10-15 00:00:00', 'développement  front -end de web service envoi des images vers back-end', NULL, NULL, 'RAS', 'RAS'),
(44, 10, 8, '2018-10-10 00:00:00', '2018-10-10 00:00:00', 'Créer les entités du projet', NULL, NULL, 'RAS', 'RAS'),
(45, 9, 5, '2018-10-16 00:00:00', '2018-10-16 00:00:00', 'test et validation', NULL, NULL, 'RAS', 'RAS'),
(46, 10, 8, '2018-10-12 00:00:00', '2018-10-12 00:00:00', 'Intégration d\'une template admin bootstrap téléchargée', NULL, NULL, 'RAS', 'RAS'),
(50, 7, 3, '2018-10-16 00:00:00', '2018-10-16 00:00:00', 'RDSUPPORT-1098', NULL, NULL, 'en cours', 'chercher le cas ou le caractéristique  Asymmetric Frame est coché'),
(51, 10, 8, '2018-10-11 00:00:00', '2018-10-11 00:00:00', 'intégration de la template dans le projet', NULL, NULL, 'téléchargement d\'une template admin bootstrap', 'Integration de la template'),
(52, 5, 4, '2018-10-01 00:00:00', '2018-10-01 00:00:00', 'RAS', NULL, NULL, 'Fitmetrix 2.0 BO', 'RAS'),
(53, 5, 7, '2018-10-08 00:00:00', '2018-10-08 00:00:00', '0', NULL, NULL, 'Brand manager (gbmonitoring)', '0'),
(54, 5, 2, '2018-10-11 00:00:00', '2018-10-11 00:00:00', 'RAS', NULL, NULL, 'gbmonitoring 3.9 :RDGBM-290', 'RAS'),
(55, 5, 10, '2018-10-12 00:00:00', '2018-10-12 00:00:00', 'RAS', NULL, NULL, 'RAS', 'RAS'),
(57, 5, 7, '2018-10-09 00:00:00', '2018-10-09 00:00:00', 'RAS', NULL, NULL, 'Brand manager (gbmonitoring)', 'RAS'),
(58, 5, 7, '2018-10-10 00:00:00', '2018-10-10 00:00:00', 'RAS', NULL, NULL, 'Brand manager (gbmonitoring)', 'RAS'),
(59, 5, 4, '2018-10-02 00:00:00', '2018-10-02 00:00:00', 'RAS', NULL, NULL, 'Fitmetrix 2.0 BO', 'RAS'),
(60, 5, 4, '2018-10-03 00:00:00', '2018-10-03 00:00:00', 'RAS', NULL, NULL, 'Fitmetrix 2.0 BO', 'RAS'),
(61, 5, 4, '2018-10-04 00:00:00', '2018-10-04 00:00:00', 'RAS', NULL, NULL, 'Fitmetrix 2.0 BO', 'RAS'),
(62, 5, 1, '2018-10-05 00:00:00', '2018-10-05 00:00:00', 'RAS', NULL, NULL, 'Fitmetrix 2.0 BO', 'RAS'),
(63, 7, 9, '2018-10-15 00:00:00', '2018-10-15 00:00:00', 'RAS', NULL, NULL, 'RAS', 'RAS'),
(64, 5, 10, '2018-10-15 00:00:00', '2018-10-15 00:00:00', 'RAS', NULL, NULL, 'RAS', 'RAS'),
(65, 5, 11, '2018-10-16 00:00:00', '2018-10-16 00:00:00', 'RAS', NULL, NULL, 'Preparation Serveur', 'RAS'),
(66, 5, 2, '2018-10-16 00:00:00', '2018-10-16 00:00:00', '1/2 J', NULL, NULL, 'gbmonitoring 3.9 :RDGBM-290', 'gbmonitoring 3.9 :RDGBM-290'),
(67, 4, 12, '2018-10-17 09:00:00', '2018-10-17 12:00:00', 'RAS', NULL, NULL, 'Faisabilité et Chiffrage Projet POST BENIN', 'RAS'),
(68, 5, 1, '2018-10-17 00:00:00', '2018-10-17 00:00:00', 'PAS de cas utiles', NULL, NULL, 'RDGBM-290', 'RDGBM-290'),
(69, 9, 5, '2018-10-17 00:00:00', '2018-10-17 00:00:00', 'snopshot Url avec httpClient', NULL, NULL, 'point avec client,\r\nListe des actions à mener (1..12)', 'Liste des actions à mener (13..24)'),
(70, 7, 3, '2018-10-17 00:00:00', '2018-10-17 00:00:00', 'RDSUPPORT-1098', NULL, NULL, 'je chercher sur une lot de frames mais j\'arrive pas à trouver une cas que le caractéristique \"Asymmetric Frame\" est coché; donc je faire une analyse de code des  fichiers \"QualificationController\" et \"RestController \" l\'attribue \"is_symmetry_active\" prend valeur \"0\" ou \"1\" en cliquant...', 'localiser ou existe le cas'),
(71, 6, 4, '2018-10-17 00:00:00', '2018-10-17 00:00:00', 'FitMertix - 28', NULL, NULL, 'résoudre le probléme du session \r\n- Désactiver la création du cookie ( Expires time , Max-age )', '- Parcourir tous les autres méthodes , et Tester l\'appel de la méthode CheckSession() dans chaque méthode du controlleur BaseController'),
(72, 10, 8, '2018-10-17 00:00:00', '2018-10-17 00:00:00', 'prbleme avec schema update', NULL, NULL, 'Installation de timesheet', 'Modélisation de l\'entité TypeCongé'),
(73, 8, 11, '2018-10-01 00:00:00', '2018-10-01 00:00:00', 'modification', NULL, NULL, 'modification', 'modification'),
(74, 8, 11, '2018-10-02 00:00:00', '2018-10-02 00:00:00', 'modification', NULL, NULL, 'modification', 'modification'),
(75, 8, 11, '2018-10-03 00:00:00', '2018-10-03 00:00:00', 'modification', NULL, NULL, 'modification', 'modification');

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_group`
--

CREATE TABLE `fos_user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `fos_user_group`
--

INSERT INTO `fos_user_group` (`id`, `name`, `roles`) VALUES
(1, 'Développeur', 'a:1:{i:0;s:16:\"ROLE_ABSHORE_DEV\";}'),
(2, 'Administrateur', 'a:1:{i:0;s:18:\"ROLE_ABSHORE_ADMIN\";}');

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_user`
--

CREATE TABLE `fos_user_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `fos_user_user`
--

INSERT INTO `fos_user_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(1, 'admin', 'admin', 'admin@base-projet.com', 'admin@base-projet.com', 1, 'f3su79xqtjscgkgck0swoowowow88gw', 'CSAgc49PQmF3rWEb81cmtX7U92EpaWh2Kkwq8g52U4tgPFivoulTxl339CldBSoaJfm13Gh+7V9HjFWE34cdFg==', '2018-10-16 12:39:49', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2018-10-12 14:16:01', '2018-10-16 12:39:49', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(4, 'ahmed.labidi@abshore.com', 'ahmed.labidi@abshore.com', 'ahmed.labidi@abshore.com', 'ahmed.labidi@abshore.com', 1, '62aed7gj19c0socwscgkks4k08wgwoc', '+3dQ3txzK4xyGkrr5UHE55h5HMOuCHgdE8mqe5SA9JghSLDlPeh/j0HJ5nyZy03c4PNvKKPhZOHnYpTW36H6fQ==', '2018-10-18 16:06:56', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2018-10-16 12:40:33', '2018-10-18 16:06:56', NULL, 'Ahmed', 'Labidi', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(5, 'zied.sfaxi@abshore.com', 'zied.sfaxi@abshore.com', 'zied.sfaxi@abshore.com', 'zied.sfaxi@abshore.com', 1, 'ss9pmvwpdg0cs4ows04gkg4swwwskc4', 'AdFsvTmcUOa3TGmeIEzkM9VMFDcGLXDoYvso10CdqnYBouaBSHgv8c5Y2/q53TC/wiZg0zElNFYsRv96KGV7eQ==', '2018-10-18 14:21:09', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2018-10-16 12:48:54', '2018-10-18 14:21:09', NULL, 'Sfaxi', 'Zied', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(6, 'nour.benarfa@abshore.com', 'nour.benarfa@abshore.com', 'nour.benarfa@abshore.com', 'nour.benarfa@abshore.com', 1, '1c8q3mxri05cwgg08wsw4cs4wkwosk4', 'GZrvux1BsrmI6b0zdejYcyU78nZXLvl9E0X50rEd/Yj+6z4/5upUn1CRuEIyfXNr8hdvRDhVQXk7Rpx0opb2GA==', '2018-10-17 18:44:25', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2018-10-16 12:49:11', '2018-10-17 18:44:25', NULL, 'Nour', 'Ben Arfa', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(7, 'marwen.kalboussi@abshore.com', 'marwen.kalboussi@abshore.com', 'marwen.kalboussi@abshore.com', 'marwen.kalboussi@abshore.com', 1, '93qsahhm1lgcccsgs0o8o4o8wok48w4', '90uAaUujrPkfuSSivN+59PWuGsOPw+lxaYeT5AXbIfYzWB0QaDiyTQnXRfKtVEW7zeQ350PwDLbVf3VEq4hGfA==', '2018-10-17 18:33:26', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2018-10-16 12:49:26', '2018-10-17 18:33:26', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(8, 'meryam.mimouni@abshore.com', 'meryam.mimouni@abshore.com', 'meryam.mimouni@abshore.com', 'meryam.mimouni@abshore.com', 1, 'r5fj5hi14i88w4kwo04sk4wo0ck08o0', 'PoYbRO22GYpjeBVou9OGK1F1CtCNQWguX1hnzgDsTVB+9eMcG4Zr6VE1KPX0Nq0Bsbr9/psj+lBXAWmPPqORMg==', '2018-10-18 15:41:08', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2018-10-16 12:49:41', '2018-10-18 15:41:08', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(9, 'slim.abdi@abshore.com', 'slim.abdi@abshore.com', 'slim.abdi@abshore.com', 'slim.abdi@abshore.com', 1, 'eitegtwwv3cooo8w4woo404kks8wowo', 'PMr1ig1qBZzl2six/zM3ErqOPl2VOErsO7O5Llnz50+YPUgrkMr+ShBn1sJcl8G77YB248J0GRRRcDJIW0I9Pw==', '2018-10-17 18:44:33', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2018-10-16 12:49:56', '2018-10-17 18:44:33', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(10, 'inched.chalbi@abshore.com', 'inched.chalbi@abshore.com', 'inched.chalbi@abshore.com', 'inched.chalbi@abshore.com', 1, 'f2bd5z1pym8040gg4soc008oskookog', 'mo+ekfQCdhbd+HMyqLKCDGgYzcVCL+uJYCYnnwyuY+hRkiy0i//Pemb3Tv43kOUHtYSL210AdiFa9cY0nZydsg==', '2018-10-17 18:47:33', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2018-10-16 12:50:09', '2018-10-17 18:47:33', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(11, 'mohamed.brini@abshore.com', 'mohamed.brini@abshore.com', 'mohamed.brini@abshore.com', 'mohamed.brini@abshore.com', 1, 'b4a6iozibxck8cooog04gw088s4w4kk', 'vV+BEpC03SgnJ+Z46g37CxZML0cIDj/I6c+Q7np32VUrWiOqZpRJ2spgCquCqliovYdgQrIrr32U1HgdhkmkYA==', '2018-10-16 16:43:42', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2018-10-16 16:43:11', '2018-10-16 16:43:42', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_user_group`
--

CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `fos_user_user_group`
--

INSERT INTO `fos_user_user_group` (`user_id`, `group_id`) VALUES
(4, 2),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 2);

-- --------------------------------------------------------

--
-- Structure de la table `media__gallery`
--

CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `media__gallery_media`
--

CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `media__media`
--

CREATE TABLE `media__media` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_identifier` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news__comment`
--

CREATE TABLE `news__comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news__post`
--

CREATE TABLE `news__post` (
  `id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `collection_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abstract` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `raw_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_formatter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_date_start` datetime DEFAULT NULL,
  `comments_enabled` tinyint(1) NOT NULL,
  `comments_close_at` datetime DEFAULT NULL,
  `comments_default_status` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news__post_tag`
--

CREATE TABLE `news__post_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `notification__message`
--

CREATE TABLE `notification__message` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `state` int(11) NOT NULL,
  `restart_count` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `project`
--

INSERT INTO `project` (`id`, `client_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'GB Monotoring 3.9', NULL, NULL),
(2, 1, 'GB Monitoring 3.10', NULL, NULL),
(3, 1, 'GBMonitoring Support', NULL, NULL),
(4, 1, 'Fitmetrix BO', NULL, NULL),
(5, 1, 'Fitmetrix FO', NULL, NULL),
(6, 1, 'Fitmix Support', NULL, NULL),
(7, 1, 'Brand Manager', NULL, NULL),
(8, 4, 'Gestion de Congés', NULL, NULL),
(9, 4, '[ Congé sans solde ]', NULL, NULL),
(10, 4, '[ Congé payé       ]', NULL, NULL),
(11, 2, 'ERP UPCAR', NULL, NULL),
(12, 4, 'Interne', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `timeline__action`
--

CREATE TABLE `timeline__action` (
  `id` int(11) NOT NULL,
  `verb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_current` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_wanted` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duplicate_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duplicate_priority` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `timeline__action`
--

INSERT INTO `timeline__action` (`id`, `verb`, `status_current`, `status_wanted`, `duplicate_key`, `duplicate_priority`, `created_at`) VALUES
(1, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-15 15:01:30'),
(2, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-15 15:01:38'),
(3, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-15 15:05:52'),
(4, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-15 15:06:04'),
(5, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-15 15:06:22'),
(6, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-15 15:43:25'),
(7, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 10:36:18'),
(8, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 10:36:48'),
(9, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 10:37:58'),
(10, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 10:38:04'),
(11, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 10:38:43'),
(12, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 10:38:48'),
(13, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:40:33'),
(14, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 12:40:38'),
(15, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:41:20'),
(16, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:41:39'),
(17, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:41:51'),
(18, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:42:19'),
(19, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:43:15'),
(20, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:43:36'),
(21, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:44:10'),
(22, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:48:54'),
(23, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:49:11'),
(24, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:49:27'),
(25, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:49:41'),
(26, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:49:57'),
(27, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 12:50:09'),
(28, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 12:50:14'),
(29, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 15:08:23'),
(30, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 15:09:30'),
(31, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 15:10:06'),
(32, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 15:10:27'),
(33, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 16:35:36'),
(34, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 16:35:44'),
(35, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 16:43:11'),
(36, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 16:43:21'),
(37, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 16:53:14'),
(38, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 16:53:39'),
(39, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 16:53:59'),
(40, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2018-10-16 18:29:43'),
(41, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-16 18:37:53'),
(42, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-17 10:34:44'),
(43, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-17 10:36:16'),
(44, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-18 16:12:18'),
(45, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-18 16:12:36'),
(46, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-18 16:12:49'),
(47, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2018-10-18 16:13:03');

-- --------------------------------------------------------

--
-- Structure de la table `timeline__action_component`
--

CREATE TABLE `timeline__action_component` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `timeline__action_component`
--

INSERT INTO `timeline__action_component` (`id`, `action_id`, `component_id`, `type`, `text`) VALUES
(1, 1, 2, 'target', NULL),
(2, 1, NULL, 'target_text', 'AppBundle\\Entity\\Client:000000001f11da6b000055f1287b097b'),
(3, 1, NULL, 'admin_code', 'admin.clients'),
(4, 1, 1, 'subject', NULL),
(5, 2, 3, 'target', NULL),
(6, 2, NULL, 'target_text', 'AppBundle\\Entity\\Client:0000000049f22ff6000055f1550adc8e'),
(7, 2, NULL, 'admin_code', 'admin.clients'),
(8, 2, 1, 'subject', NULL),
(9, 3, 4, 'target', NULL),
(10, 3, NULL, 'target_text', 'GB Monotoring 3.9'),
(11, 3, NULL, 'admin_code', 'admin.projects'),
(12, 3, 1, 'subject', NULL),
(13, 4, 5, 'target', NULL),
(14, 4, NULL, 'target_text', 'GB Monitoring 3.10'),
(15, 4, NULL, 'admin_code', 'admin.projects'),
(16, 4, 1, 'subject', NULL),
(17, 5, 6, 'target', NULL),
(18, 5, NULL, 'target_text', 'GBMonitoring Support'),
(19, 5, NULL, 'admin_code', 'admin.projects'),
(20, 5, 1, 'subject', NULL),
(21, 6, 7, 'target', NULL),
(22, 6, NULL, 'target_text', '-'),
(23, 6, NULL, 'admin_code', 'admin.events'),
(24, 6, 1, 'subject', NULL),
(25, 7, 8, 'target', NULL),
(26, 7, NULL, 'target_text', 'Développeur'),
(27, 7, NULL, 'admin_code', 'sonata.user.admin.group'),
(28, 7, 1, 'subject', NULL),
(29, 8, 9, 'target', NULL),
(30, 8, NULL, 'target_text', 'Administrateur'),
(31, 8, NULL, 'admin_code', 'sonata.user.admin.group'),
(32, 8, 1, 'subject', NULL),
(33, 9, 10, 'target', NULL),
(34, 9, NULL, 'target_text', 'administrateur'),
(35, 9, NULL, 'admin_code', 'sonata.user.admin.user'),
(36, 9, 1, 'subject', NULL),
(37, 10, 10, 'target', NULL),
(38, 10, NULL, 'target_text', 'administrateur'),
(39, 10, NULL, 'admin_code', 'sonata.user.admin.user'),
(40, 10, 1, 'subject', NULL),
(41, 11, 11, 'target', NULL),
(42, 11, NULL, 'target_text', 'zs'),
(43, 11, NULL, 'admin_code', 'sonata.user.admin.user'),
(44, 11, 1, 'subject', NULL),
(45, 12, 11, 'target', NULL),
(46, 12, NULL, 'target_text', 'zs'),
(47, 12, NULL, 'admin_code', 'sonata.user.admin.user'),
(48, 12, 1, 'subject', NULL),
(49, 13, 12, 'target', NULL),
(50, 13, NULL, 'target_text', 'ahmed.labidi@abshore.com'),
(51, 13, NULL, 'admin_code', 'sonata.user.admin.user'),
(52, 13, 1, 'subject', NULL),
(53, 14, 12, 'target', NULL),
(54, 14, NULL, 'target_text', 'ahmed.labidi@abshore.com'),
(55, 14, NULL, 'admin_code', 'sonata.user.admin.user'),
(56, 14, 1, 'subject', NULL),
(57, 15, 13, 'target', NULL),
(58, 15, NULL, 'target_text', 'C\'EVIDENTIA'),
(59, 15, NULL, 'admin_code', 'admin.clients'),
(60, 15, 12, 'subject', NULL),
(61, 16, 14, 'target', NULL),
(62, 16, NULL, 'target_text', 'Fitmetrix BO'),
(63, 16, NULL, 'admin_code', 'admin.projects'),
(64, 16, 12, 'subject', NULL),
(65, 17, 15, 'target', NULL),
(66, 17, NULL, 'target_text', 'Fitmetrix FO'),
(67, 17, NULL, 'admin_code', 'admin.projects'),
(68, 17, 12, 'subject', NULL),
(69, 18, 16, 'target', NULL),
(70, 18, NULL, 'target_text', 'Fitmix Support'),
(71, 18, NULL, 'admin_code', 'admin.projects'),
(72, 18, 12, 'subject', NULL),
(73, 19, 17, 'target', NULL),
(74, 19, NULL, 'target_text', 'Brand Manager'),
(75, 19, NULL, 'admin_code', 'admin.projects'),
(76, 19, 12, 'subject', NULL),
(77, 20, 18, 'target', NULL),
(78, 20, NULL, 'target_text', 'ABSHORE'),
(79, 20, NULL, 'admin_code', 'admin.clients'),
(80, 20, 12, 'subject', NULL),
(81, 21, 19, 'target', NULL),
(82, 21, NULL, 'target_text', 'Congé'),
(83, 21, NULL, 'admin_code', 'admin.projects'),
(84, 21, 12, 'subject', NULL),
(85, 22, 20, 'target', NULL),
(86, 22, NULL, 'target_text', 'zied.sfaxi@abshore.com'),
(87, 22, NULL, 'admin_code', 'sonata.user.admin.user'),
(88, 22, 12, 'subject', NULL),
(89, 23, 21, 'target', NULL),
(90, 23, NULL, 'target_text', 'nour.benarfa@abshore.com'),
(91, 23, NULL, 'admin_code', 'sonata.user.admin.user'),
(92, 23, 12, 'subject', NULL),
(93, 24, 22, 'target', NULL),
(94, 24, NULL, 'target_text', 'marwen.kalboussi@abshore.com'),
(95, 24, NULL, 'admin_code', 'sonata.user.admin.user'),
(96, 24, 12, 'subject', NULL),
(97, 25, 23, 'target', NULL),
(98, 25, NULL, 'target_text', 'meryam.mimouni@abshore.com'),
(99, 25, NULL, 'admin_code', 'sonata.user.admin.user'),
(100, 25, 12, 'subject', NULL),
(101, 26, 24, 'target', NULL),
(102, 26, NULL, 'target_text', 'slim.abdi@abshore.com'),
(103, 26, NULL, 'admin_code', 'sonata.user.admin.user'),
(104, 26, 12, 'subject', NULL),
(105, 27, 25, 'target', NULL),
(106, 27, NULL, 'target_text', 'inched.chalbi@abshore.com'),
(107, 27, NULL, 'admin_code', 'sonata.user.admin.user'),
(108, 27, 12, 'subject', NULL),
(109, 28, 25, 'target', NULL),
(110, 28, NULL, 'target_text', 'inched.chalbi@abshore.com'),
(111, 28, NULL, 'admin_code', 'sonata.user.admin.user'),
(112, 28, 12, 'subject', NULL),
(113, 29, 25, 'target', NULL),
(114, 29, NULL, 'target_text', 'inched.chalbi@abshore.com'),
(115, 29, NULL, 'admin_code', 'sonata.user.admin.user'),
(116, 29, 12, 'subject', NULL),
(117, 30, 19, 'target', NULL),
(118, 30, NULL, 'target_text', 'Gestion de Congés'),
(119, 30, NULL, 'admin_code', 'admin.projects'),
(120, 30, 12, 'subject', NULL),
(121, 31, 26, 'target', NULL),
(122, 31, NULL, 'target_text', '======== Congé sans solde ========'),
(123, 31, NULL, 'admin_code', 'admin.projects'),
(124, 31, 12, 'subject', NULL),
(125, 32, 27, 'target', NULL),
(126, 32, NULL, 'target_text', '======== Congé payé ========'),
(127, 32, NULL, 'admin_code', 'admin.projects'),
(128, 32, 12, 'subject', NULL),
(129, 33, 26, 'target', NULL),
(130, 33, NULL, 'target_text', '[ Congé sans solde ]'),
(131, 33, NULL, 'admin_code', 'admin.projects'),
(132, 33, 12, 'subject', NULL),
(133, 34, 27, 'target', NULL),
(134, 34, NULL, 'target_text', '[ Congé payé       ]'),
(135, 34, NULL, 'admin_code', 'admin.projects'),
(136, 34, 12, 'subject', NULL),
(137, 35, 28, 'target', NULL),
(138, 35, NULL, 'target_text', 'mohamed.brini@abshore.com'),
(139, 35, NULL, 'admin_code', 'sonata.user.admin.user'),
(140, 35, 12, 'subject', NULL),
(141, 36, 28, 'target', NULL),
(142, 36, NULL, 'target_text', 'mohamed.brini@abshore.com'),
(143, 36, NULL, 'admin_code', 'sonata.user.admin.user'),
(144, 36, 12, 'subject', NULL),
(145, 37, 12, 'target', NULL),
(146, 37, NULL, 'target_text', 'ahmed.labidi@abshore.com'),
(147, 37, NULL, 'admin_code', 'sonata.user.admin.user'),
(148, 37, 12, 'subject', NULL),
(149, 38, 20, 'target', NULL),
(150, 38, NULL, 'target_text', 'zied.sfaxi@abshore.com'),
(151, 38, NULL, 'admin_code', 'sonata.user.admin.user'),
(152, 38, 12, 'subject', NULL),
(153, 39, 21, 'target', NULL),
(154, 39, NULL, 'target_text', 'nour.benarfa@abshore.com'),
(155, 39, NULL, 'admin_code', 'sonata.user.admin.user'),
(156, 39, 12, 'subject', NULL),
(157, 40, 29, 'target', NULL),
(158, 40, NULL, 'target_text', 'marwen.kalboussi@abshore.com'),
(159, 40, NULL, 'admin_code', 'admin.events'),
(160, 40, 12, 'subject', NULL),
(161, 41, 30, 'target', NULL),
(162, 41, NULL, 'target_text', 'ERP UPCAR'),
(163, 41, NULL, 'admin_code', 'admin.projects'),
(164, 41, 12, 'subject', NULL),
(165, 42, 31, 'target', NULL),
(166, 42, NULL, 'target_text', 'Interne'),
(167, 42, NULL, 'admin_code', 'admin.projects'),
(168, 42, 12, 'subject', NULL),
(169, 43, 32, 'target', NULL),
(170, 43, NULL, 'target_text', 'ahmed.labidi@abshore.com'),
(171, 43, NULL, 'admin_code', 'admin.events'),
(172, 43, 12, 'subject', NULL),
(173, 44, 33, 'target', NULL),
(174, 44, NULL, 'target_text', 'Maladie'),
(175, 44, NULL, 'admin_code', 'admin.type_conges'),
(176, 44, 12, 'subject', NULL),
(177, 45, 34, 'target', NULL),
(178, 45, NULL, 'target_text', 'Maternité'),
(179, 45, NULL, 'admin_code', 'admin.type_conges'),
(180, 45, 12, 'subject', NULL),
(181, 46, 35, 'target', NULL),
(182, 46, NULL, 'target_text', 'Payé'),
(183, 46, NULL, 'admin_code', 'admin.type_conges'),
(184, 46, 12, 'subject', NULL),
(185, 47, 36, 'target', NULL),
(186, 47, NULL, 'target_text', 'Sans solde'),
(187, 47, NULL, 'admin_code', 'admin.type_conges'),
(188, 47, 12, 'subject', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `timeline__component`
--

CREATE TABLE `timeline__component` (
  `id` int(11) NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `timeline__component`
--

INSERT INTO `timeline__component` (`id`, `model`, `identifier`, `hash`) VALUES
(1, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"1\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"1\";'),
(2, 'AppBundle\\Entity\\Client', 's:1:\"1\";', 'AppBundle\\Entity\\Client#s:1:\"1\";'),
(3, 'AppBundle\\Entity\\Client', 's:1:\"2\";', 'AppBundle\\Entity\\Client#s:1:\"2\";'),
(4, 'AppBundle\\Entity\\Project', 's:1:\"1\";', 'AppBundle\\Entity\\Project#s:1:\"1\";'),
(5, 'AppBundle\\Entity\\Project', 's:1:\"2\";', 'AppBundle\\Entity\\Project#s:1:\"2\";'),
(6, 'AppBundle\\Entity\\Project', 's:1:\"3\";', 'AppBundle\\Entity\\Project#s:1:\"3\";'),
(7, 'AppBundle\\Entity\\Event', 's:1:\"1\";', 'AppBundle\\Entity\\Event#s:1:\"1\";'),
(8, 'Application\\Sonata\\UserBundle\\Entity\\Group', 's:1:\"1\";', 'Application\\Sonata\\UserBundle\\Entity\\Group#s:1:\"1\";'),
(9, 'Application\\Sonata\\UserBundle\\Entity\\Group', 's:1:\"2\";', 'Application\\Sonata\\UserBundle\\Entity\\Group#s:1:\"2\";'),
(10, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"2\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"2\";'),
(11, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"3\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"3\";'),
(12, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"4\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"4\";'),
(13, 'AppBundle\\Entity\\Client', 's:1:\"3\";', 'AppBundle\\Entity\\Client#s:1:\"3\";'),
(14, 'AppBundle\\Entity\\Project', 's:1:\"4\";', 'AppBundle\\Entity\\Project#s:1:\"4\";'),
(15, 'AppBundle\\Entity\\Project', 's:1:\"5\";', 'AppBundle\\Entity\\Project#s:1:\"5\";'),
(16, 'AppBundle\\Entity\\Project', 's:1:\"6\";', 'AppBundle\\Entity\\Project#s:1:\"6\";'),
(17, 'AppBundle\\Entity\\Project', 's:1:\"7\";', 'AppBundle\\Entity\\Project#s:1:\"7\";'),
(18, 'AppBundle\\Entity\\Client', 's:1:\"4\";', 'AppBundle\\Entity\\Client#s:1:\"4\";'),
(19, 'AppBundle\\Entity\\Project', 's:1:\"8\";', 'AppBundle\\Entity\\Project#s:1:\"8\";'),
(20, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"5\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"5\";'),
(21, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"6\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"6\";'),
(22, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"7\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"7\";'),
(23, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"8\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"8\";'),
(24, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:1:\"9\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:1:\"9\";'),
(25, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:2:\"10\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:2:\"10\";'),
(26, 'AppBundle\\Entity\\Project', 's:1:\"9\";', 'AppBundle\\Entity\\Project#s:1:\"9\";'),
(27, 'AppBundle\\Entity\\Project', 's:2:\"10\";', 'AppBundle\\Entity\\Project#s:2:\"10\";'),
(28, 'Application\\Sonata\\UserBundle\\Entity\\User', 's:2:\"11\";', 'Application\\Sonata\\UserBundle\\Entity\\User#s:2:\"11\";'),
(29, 'AppBundle\\Entity\\Event', 's:2:\"63\";', 'AppBundle\\Entity\\Event#s:2:\"63\";'),
(30, 'AppBundle\\Entity\\Project', 's:2:\"11\";', 'AppBundle\\Entity\\Project#s:2:\"11\";'),
(31, 'AppBundle\\Entity\\Project', 's:2:\"12\";', 'AppBundle\\Entity\\Project#s:2:\"12\";'),
(32, 'AppBundle\\Entity\\Event', 's:2:\"67\";', 'AppBundle\\Entity\\Event#s:2:\"67\";'),
(33, 'AppBundle\\Entity\\TypeConge', 's:1:\"1\";', 'AppBundle\\Entity\\TypeConge#s:1:\"1\";'),
(34, 'AppBundle\\Entity\\TypeConge', 's:1:\"2\";', 'AppBundle\\Entity\\TypeConge#s:1:\"2\";'),
(35, 'AppBundle\\Entity\\TypeConge', 's:1:\"3\";', 'AppBundle\\Entity\\TypeConge#s:1:\"3\";'),
(36, 'AppBundle\\Entity\\TypeConge', 's:1:\"4\";', 'AppBundle\\Entity\\TypeConge#s:1:\"4\";');

-- --------------------------------------------------------

--
-- Structure de la table `timeline__timeline`
--

CREATE TABLE `timeline__timeline` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `timeline__timeline`
--

INSERT INTO `timeline__timeline` (`id`, `action_id`, `subject_id`, `context`, `type`, `created_at`) VALUES
(1, 1, 1, 'GLOBAL', 'timeline', '2018-10-15 15:01:30'),
(2, 1, 1, 'SONATA_ADMIN', 'timeline', '2018-10-15 15:01:30'),
(3, 2, 1, 'GLOBAL', 'timeline', '2018-10-15 15:01:38'),
(4, 2, 1, 'SONATA_ADMIN', 'timeline', '2018-10-15 15:01:38'),
(5, 3, 1, 'GLOBAL', 'timeline', '2018-10-15 15:05:52'),
(6, 3, 1, 'SONATA_ADMIN', 'timeline', '2018-10-15 15:05:52'),
(7, 4, 1, 'GLOBAL', 'timeline', '2018-10-15 15:06:04'),
(8, 4, 1, 'SONATA_ADMIN', 'timeline', '2018-10-15 15:06:04'),
(9, 5, 1, 'GLOBAL', 'timeline', '2018-10-15 15:06:22'),
(10, 5, 1, 'SONATA_ADMIN', 'timeline', '2018-10-15 15:06:22'),
(11, 6, 1, 'GLOBAL', 'timeline', '2018-10-15 15:43:25'),
(12, 6, 1, 'SONATA_ADMIN', 'timeline', '2018-10-15 15:43:25'),
(13, 7, 1, 'GLOBAL', 'timeline', '2018-10-16 10:36:19'),
(14, 7, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 10:36:19'),
(15, 8, 1, 'GLOBAL', 'timeline', '2018-10-16 10:36:49'),
(16, 8, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 10:36:49'),
(17, 9, 1, 'GLOBAL', 'timeline', '2018-10-16 10:37:58'),
(18, 9, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 10:37:58'),
(19, 10, 1, 'GLOBAL', 'timeline', '2018-10-16 10:38:04'),
(20, 10, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 10:38:04'),
(21, 11, 1, 'GLOBAL', 'timeline', '2018-10-16 10:38:43'),
(22, 11, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 10:38:43'),
(23, 12, 1, 'GLOBAL', 'timeline', '2018-10-16 10:38:48'),
(24, 12, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 10:38:48'),
(25, 13, 1, 'GLOBAL', 'timeline', '2018-10-16 12:40:34'),
(26, 13, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:40:34'),
(27, 14, 1, 'GLOBAL', 'timeline', '2018-10-16 12:40:39'),
(28, 14, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:40:39'),
(29, 15, 12, 'GLOBAL', 'timeline', '2018-10-16 12:41:20'),
(30, 15, 1, 'GLOBAL', 'timeline', '2018-10-16 12:41:20'),
(31, 15, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:41:20'),
(32, 16, 12, 'GLOBAL', 'timeline', '2018-10-16 12:41:39'),
(33, 16, 1, 'GLOBAL', 'timeline', '2018-10-16 12:41:39'),
(34, 16, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:41:39'),
(35, 17, 12, 'GLOBAL', 'timeline', '2018-10-16 12:41:51'),
(36, 17, 1, 'GLOBAL', 'timeline', '2018-10-16 12:41:51'),
(37, 17, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:41:51'),
(38, 18, 12, 'GLOBAL', 'timeline', '2018-10-16 12:42:19'),
(39, 18, 1, 'GLOBAL', 'timeline', '2018-10-16 12:42:19'),
(40, 18, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:42:19'),
(41, 19, 12, 'GLOBAL', 'timeline', '2018-10-16 12:43:15'),
(42, 19, 1, 'GLOBAL', 'timeline', '2018-10-16 12:43:15'),
(43, 19, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:43:15'),
(44, 20, 12, 'GLOBAL', 'timeline', '2018-10-16 12:43:36'),
(45, 20, 1, 'GLOBAL', 'timeline', '2018-10-16 12:43:36'),
(46, 20, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:43:36'),
(47, 21, 12, 'GLOBAL', 'timeline', '2018-10-16 12:44:10'),
(48, 21, 1, 'GLOBAL', 'timeline', '2018-10-16 12:44:10'),
(49, 21, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:44:10'),
(50, 22, 12, 'GLOBAL', 'timeline', '2018-10-16 12:48:54'),
(51, 22, 1, 'GLOBAL', 'timeline', '2018-10-16 12:48:54'),
(52, 22, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:48:54'),
(53, 23, 12, 'GLOBAL', 'timeline', '2018-10-16 12:49:12'),
(54, 23, 1, 'GLOBAL', 'timeline', '2018-10-16 12:49:12'),
(55, 23, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:49:12'),
(56, 24, 12, 'GLOBAL', 'timeline', '2018-10-16 12:49:27'),
(57, 24, 1, 'GLOBAL', 'timeline', '2018-10-16 12:49:27'),
(58, 24, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:49:27'),
(59, 25, 12, 'GLOBAL', 'timeline', '2018-10-16 12:49:41'),
(60, 25, 1, 'GLOBAL', 'timeline', '2018-10-16 12:49:41'),
(61, 25, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:49:41'),
(62, 26, 12, 'GLOBAL', 'timeline', '2018-10-16 12:49:57'),
(63, 26, 1, 'GLOBAL', 'timeline', '2018-10-16 12:49:57'),
(64, 26, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:49:57'),
(65, 27, 12, 'GLOBAL', 'timeline', '2018-10-16 12:50:09'),
(66, 27, 1, 'GLOBAL', 'timeline', '2018-10-16 12:50:09'),
(67, 27, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:50:09'),
(68, 28, 12, 'GLOBAL', 'timeline', '2018-10-16 12:50:15'),
(69, 28, 1, 'GLOBAL', 'timeline', '2018-10-16 12:50:15'),
(70, 28, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 12:50:15'),
(71, 29, 12, 'GLOBAL', 'timeline', '2018-10-16 15:08:23'),
(72, 29, 1, 'GLOBAL', 'timeline', '2018-10-16 15:08:23'),
(73, 29, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 15:08:23'),
(74, 30, 12, 'GLOBAL', 'timeline', '2018-10-16 15:09:30'),
(75, 30, 1, 'GLOBAL', 'timeline', '2018-10-16 15:09:30'),
(76, 30, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 15:09:30'),
(77, 31, 12, 'GLOBAL', 'timeline', '2018-10-16 15:10:06'),
(78, 31, 1, 'GLOBAL', 'timeline', '2018-10-16 15:10:06'),
(79, 31, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 15:10:06'),
(80, 32, 12, 'GLOBAL', 'timeline', '2018-10-16 15:10:27'),
(81, 32, 1, 'GLOBAL', 'timeline', '2018-10-16 15:10:27'),
(82, 32, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 15:10:27'),
(83, 33, 12, 'GLOBAL', 'timeline', '2018-10-16 16:35:37'),
(84, 33, 1, 'GLOBAL', 'timeline', '2018-10-16 16:35:37'),
(85, 33, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 16:35:37'),
(86, 34, 12, 'GLOBAL', 'timeline', '2018-10-16 16:35:44'),
(87, 34, 1, 'GLOBAL', 'timeline', '2018-10-16 16:35:44'),
(88, 34, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 16:35:44'),
(89, 35, 12, 'GLOBAL', 'timeline', '2018-10-16 16:43:11'),
(90, 35, 1, 'GLOBAL', 'timeline', '2018-10-16 16:43:11'),
(91, 35, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 16:43:11'),
(92, 36, 12, 'GLOBAL', 'timeline', '2018-10-16 16:43:21'),
(93, 36, 1, 'GLOBAL', 'timeline', '2018-10-16 16:43:21'),
(94, 36, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 16:43:21'),
(95, 37, 12, 'GLOBAL', 'timeline', '2018-10-16 16:53:14'),
(96, 37, 1, 'GLOBAL', 'timeline', '2018-10-16 16:53:14'),
(97, 37, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 16:53:14'),
(98, 38, 12, 'GLOBAL', 'timeline', '2018-10-16 16:53:39'),
(99, 38, 1, 'GLOBAL', 'timeline', '2018-10-16 16:53:39'),
(100, 38, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 16:53:39'),
(101, 39, 12, 'GLOBAL', 'timeline', '2018-10-16 16:53:59'),
(102, 39, 1, 'GLOBAL', 'timeline', '2018-10-16 16:53:59'),
(103, 39, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 16:53:59'),
(104, 40, 12, 'GLOBAL', 'timeline', '2018-10-16 18:29:43'),
(105, 40, 1, 'GLOBAL', 'timeline', '2018-10-16 18:29:43'),
(106, 40, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 18:29:43'),
(107, 41, 12, 'GLOBAL', 'timeline', '2018-10-16 18:37:53'),
(108, 41, 1, 'GLOBAL', 'timeline', '2018-10-16 18:37:53'),
(109, 41, 1, 'SONATA_ADMIN', 'timeline', '2018-10-16 18:37:53'),
(110, 42, 12, 'GLOBAL', 'timeline', '2018-10-17 10:34:44'),
(111, 42, 1, 'GLOBAL', 'timeline', '2018-10-17 10:34:44'),
(112, 42, 1, 'SONATA_ADMIN', 'timeline', '2018-10-17 10:34:44'),
(113, 43, 12, 'GLOBAL', 'timeline', '2018-10-17 10:36:16'),
(114, 43, 1, 'GLOBAL', 'timeline', '2018-10-17 10:36:16'),
(115, 43, 1, 'SONATA_ADMIN', 'timeline', '2018-10-17 10:36:16'),
(116, 44, 12, 'GLOBAL', 'timeline', '2018-10-18 16:12:18'),
(117, 44, 1, 'GLOBAL', 'timeline', '2018-10-18 16:12:18'),
(118, 44, 1, 'SONATA_ADMIN', 'timeline', '2018-10-18 16:12:18'),
(119, 45, 12, 'GLOBAL', 'timeline', '2018-10-18 16:12:36'),
(120, 45, 1, 'GLOBAL', 'timeline', '2018-10-18 16:12:36'),
(121, 45, 1, 'SONATA_ADMIN', 'timeline', '2018-10-18 16:12:36'),
(122, 46, 12, 'GLOBAL', 'timeline', '2018-10-18 16:12:49'),
(123, 46, 1, 'GLOBAL', 'timeline', '2018-10-18 16:12:49'),
(124, 46, 1, 'SONATA_ADMIN', 'timeline', '2018-10-18 16:12:49'),
(125, 47, 12, 'GLOBAL', 'timeline', '2018-10-18 16:13:03'),
(126, 47, 1, 'GLOBAL', 'timeline', '2018-10-18 16:13:03'),
(127, 47, 1, 'SONATA_ADMIN', 'timeline', '2018-10-18 16:13:03');

-- --------------------------------------------------------

--
-- Structure de la table `type_conge`
--

CREATE TABLE `type_conge` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `type_conge`
--

INSERT INTO `type_conge` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Maladie', NULL, NULL),
(2, 'Maternité', NULL, NULL),
(3, 'Payé', NULL, NULL),
(4, 'Sans solde', NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Index pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Index pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Index pour la table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Index pour la table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Index pour la table `classification__category`
--
ALTER TABLE `classification__category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_43629B36727ACA70` (`parent_id`),
  ADD KEY `IDX_43629B36E25D857E` (`context`),
  ADD KEY `IDX_43629B36EA9FDD75` (`media_id`);

--
-- Index pour la table `classification__collection`
--
ALTER TABLE `classification__collection`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_collection` (`slug`,`context`),
  ADD KEY `IDX_A406B56AE25D857E` (`context`),
  ADD KEY `IDX_A406B56AEA9FDD75` (`media_id`);

--
-- Index pour la table `classification__context`
--
ALTER TABLE `classification__context`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `classification__tag`
--
ALTER TABLE `classification__tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_context` (`slug`,`context`),
  ADD KEY `IDX_CA57A1C7E25D857E` (`context`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `conge`
--
ALTER TABLE `conge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2ED89348C54C8C93` (`type_id`),
  ADD KEY `IDX_2ED89348A76ED395` (`user_id`);

--
-- Index pour la table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3BAE0AA76B3CA4B` (`user_id`),
  ADD KEY `IDX_3BAE0AA79122A03F` (`project_id`);

--
-- Index pour la table `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`);

--
-- Index pour la table `fos_user_user`
--
ALTER TABLE `fos_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`);

--
-- Index pour la table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Index pour la table `media__gallery`
--
ALTER TABLE `media__gallery`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  ADD KEY `IDX_80D4C541EA9FDD75` (`media_id`);

--
-- Index pour la table `media__media`
--
ALTER TABLE `media__media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5C6DD74E12469DE2` (`category_id`);

--
-- Index pour la table `news__comment`
--
ALTER TABLE `news__comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A90210404B89032C` (`post_id`);

--
-- Index pour la table `news__post`
--
ALTER TABLE `news__post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7D109BC83DA5256D` (`image_id`),
  ADD KEY `IDX_7D109BC8F675F31B` (`author_id`),
  ADD KEY `IDX_7D109BC8514956FD` (`collection_id`);

--
-- Index pour la table `news__post_tag`
--
ALTER TABLE `news__post_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `IDX_682B20514B89032C` (`post_id`),
  ADD KEY `IDX_682B2051BAD26311` (`tag_id`);

--
-- Index pour la table `notification__message`
--
ALTER TABLE `notification__message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_message_state_idx` (`state`),
  ADD KEY `notification_message_created_at_idx` (`created_at`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_at` (`created_at`);

--
-- Index pour la table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2FB3D0EE19EB6921` (`client_id`);

--
-- Index pour la table `timeline__action`
--
ALTER TABLE `timeline__action`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `timeline__action_component`
--
ALTER TABLE `timeline__action_component`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6ACD1B169D32F035` (`action_id`),
  ADD KEY `IDX_6ACD1B16E2ABAFFF` (`component_id`);

--
-- Index pour la table `timeline__component`
--
ALTER TABLE `timeline__component`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1B2F01CDD1B862B8` (`hash`);

--
-- Index pour la table `timeline__timeline`
--
ALTER TABLE `timeline__timeline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FFBC6AD59D32F035` (`action_id`),
  ADD KEY `IDX_FFBC6AD523EDC87` (`subject_id`),
  ADD KEY `context_idx` (`context`),
  ADD KEY `type_idx` (`type`);

--
-- Index pour la table `type_conge`
--
ALTER TABLE `type_conge`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT pour la table `classification__category`
--
ALTER TABLE `classification__category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `classification__collection`
--
ALTER TABLE `classification__collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `classification__tag`
--
ALTER TABLE `classification__tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `conge`
--
ALTER TABLE `conge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT pour la table `fos_user_group`
--
ALTER TABLE `fos_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `fos_user_user`
--
ALTER TABLE `fos_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `media__gallery`
--
ALTER TABLE `media__gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `media__media`
--
ALTER TABLE `media__media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `news__comment`
--
ALTER TABLE `news__comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `news__post`
--
ALTER TABLE `news__post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `notification__message`
--
ALTER TABLE `notification__message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `timeline__action`
--
ALTER TABLE `timeline__action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT pour la table `timeline__action_component`
--
ALTER TABLE `timeline__action_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;
--
-- AUTO_INCREMENT pour la table `timeline__component`
--
ALTER TABLE `timeline__component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pour la table `timeline__timeline`
--
ALTER TABLE `timeline__timeline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT pour la table `type_conge`
--
ALTER TABLE `type_conge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Contraintes pour la table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `classification__category`
--
ALTER TABLE `classification__category`
  ADD CONSTRAINT `FK_43629B36727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `classification__category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_43629B36E25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`),
  ADD CONSTRAINT `FK_43629B36EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `classification__collection`
--
ALTER TABLE `classification__collection`
  ADD CONSTRAINT `FK_A406B56AE25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`),
  ADD CONSTRAINT `FK_A406B56AEA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `classification__tag`
--
ALTER TABLE `classification__tag`
  ADD CONSTRAINT `FK_CA57A1C7E25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`);

--
-- Contraintes pour la table `conge`
--
ALTER TABLE `conge`
  ADD CONSTRAINT `FK_2ED89348A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`),
  ADD CONSTRAINT `FK_2ED89348C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `type_conge` (`id`);

--
-- Contraintes pour la table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FK_3BAE0AA7166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `FK_3BAE0AA7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`);

--
-- Contraintes pour la table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `media__media`
--
ALTER TABLE `media__media`
  ADD CONSTRAINT `FK_5C6DD74E12469DE2` FOREIGN KEY (`category_id`) REFERENCES `classification__category` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `news__comment`
--
ALTER TABLE `news__comment`
  ADD CONSTRAINT `FK_A90210404B89032C` FOREIGN KEY (`post_id`) REFERENCES `news__post` (`id`);

--
-- Contraintes pour la table `news__post`
--
ALTER TABLE `news__post`
  ADD CONSTRAINT `FK_7D109BC83DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media__media` (`id`),
  ADD CONSTRAINT `FK_7D109BC8514956FD` FOREIGN KEY (`collection_id`) REFERENCES `classification__collection` (`id`),
  ADD CONSTRAINT `FK_7D109BC8F675F31B` FOREIGN KEY (`author_id`) REFERENCES `fos_user_user` (`id`);

--
-- Contraintes pour la table `news__post_tag`
--
ALTER TABLE `news__post_tag`
  ADD CONSTRAINT `FK_682B20514B89032C` FOREIGN KEY (`post_id`) REFERENCES `news__post` (`id`),
  ADD CONSTRAINT `FK_682B2051BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `classification__tag` (`id`);

--
-- Contraintes pour la table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `FK_2FB3D0EE19EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `timeline__action_component`
--
ALTER TABLE `timeline__action_component`
  ADD CONSTRAINT `FK_6ACD1B169D32F035` FOREIGN KEY (`action_id`) REFERENCES `timeline__action` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6ACD1B16E2ABAFFF` FOREIGN KEY (`component_id`) REFERENCES `timeline__component` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `timeline__timeline`
--
ALTER TABLE `timeline__timeline`
  ADD CONSTRAINT `FK_FFBC6AD523EDC87` FOREIGN KEY (`subject_id`) REFERENCES `timeline__component` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FFBC6AD59D32F035` FOREIGN KEY (`action_id`) REFERENCES `timeline__action` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
