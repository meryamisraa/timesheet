<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'ZendDiagnostics\\' => array($vendorDir . '/zendframework/zenddiagnostics/src'),
    'ZendDiagnosticsTest\\' => array($vendorDir . '/zendframework/zenddiagnostics/tests'),
    'Twig_Extensions_' => array($vendorDir . '/twig/extensions/lib'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Sensio\\Bundle\\DistributionBundle' => array($vendorDir . '/sensio/distribution-bundle'),
    'SensioLabs\\Security' => array($vendorDir . '/sensiolabs/security-checker'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src'),
    'PhpCollection' => array($vendorDir . '/phpcollection/phpcollection/src'),
    'Metadata\\' => array($vendorDir . '/jms/metadata/src'),
    'Knp\\Component' => array($vendorDir . '/knplabs/knp-components/src'),
    'JsonpCallbackValidator' => array($vendorDir . '/willdurand/jsonp-callback-validator/src'),
    'JMS\\Serializer' => array($vendorDir . '/jms/serializer/src'),
    'JMS\\' => array($vendorDir . '/jms/parser-lib/src'),
    'Imagine' => array($vendorDir . '/imagine/imagine/lib'),
    'Gaufrette' => array($vendorDir . '/knplabs/gaufrette/src'),
    'FOS\\UserBundle' => array($vendorDir . '/friendsofsymfony/user-bundle'),
    'FOS\\RestBundle' => array($vendorDir . '/friendsofsymfony/rest-bundle'),
    'Doctrine\\ORM\\' => array($vendorDir . '/doctrine/orm/lib'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'CoopTilleuls\\Bundle\\AclSonataAdminExtensionBundle' => array($vendorDir . '/tilleuls/acl-sonata-admin-extension-bundle'),
);
